# Final Project

Hej! 

Welcome to the "final project" section. Here, I'll document every step I took to get to the final project!

## Design or not Design?
Before we jump into the object, I'd like to briefly talk about the little design course our professor Victor Levy held for us this mourning.

Have you ever had a feeling, that you've been working on a project for so long, that you know it by heart, but when someone asks you the most simple question about your work, you just become numb and don't know how to answer? Well, you are not alone! Explaining what design is, or even defining the term "design" is more complex than it seems.

We can start by saying that throughout history, design always played a big part in humans. Many films integrate design, such as "Mon Oncle" (my uncle) by Jacques Tati. Or even books "Learning from Las Vegas" from Denise Scott Brown, Robert Venturi, and Steven Izenour, that talk about the absurdity of the architecture of Las Vegas. And just like there are absurd buildings in Architecture, is there an "absurd Design"? Yes!

The most important thing to remember about design is that human beings explore new forms in specific objects, which they need a solution to. This requires for the object to meet a certain standard, so they need a certain function. To summarize, there are two main objects in this world:
1. The first one, is well analyzed and thought out. These are our "everyday objects", that are so discreet, we as users, don't notice them most of the time. I tend to say good, functional objects tend to go unnoticed.
2. The other type of object does the total opposite. They tend to get on people’s nerves and are not suited for the human body. These objects don't have a useful function, but they have something more powerful. They get people to talk and start a dialogue. (See weird Japanese inventions)

What these two objects have in common, is the curiosity it has on people as a whole. You might be the type of person, who wants to have a solution for everything and that's fine. But you might also be enthusiastic who wants to explore and get people to talk about a specific kind of topic. Whatever the "type" of designer you are, check out this TED Talk video [Why you should make useless things | Simone Giertz](https://www.youtube.com/watch?v=c0bsKc4tiuY). The point is that building stupid things is quite smart.

## Bräda Laptop holder
Now, let's talk about the object!
Just for reminder, the object I chose to work on this year is a Laptop holder, from the Swedish company IKEA. It’s called Bräda and it translates to "Board" (from Swedish to English). Ikea always had a funny way of naming its products.

This object was bought a few years ago, back when I was in high school and I needed a stand to put my big and super-wide laptop. Back then, laptops were not so thin and narrow as we know them nowadays. Standard measurements for laptops go around from 11 inches (around 27,94cm) to 17 inches (around 43,18cm). 

As you might know by now, IKEA is a well-established and worldwide known Swedish mass-producing company. What's so interesting and attractive about IKEA, is its affordable prices. They don't just focus on one particular type of consumer, because they have everything at accessible prices, for low-income families or high-income families, for students, etc. [This video](https://www.youtube.com/watch?v=za8FOavztZQ) really explains perfectly how IKEA works and also why we get so easily hooked.

I thought this Laptop holder wasn't available anymore since it's such a long time I have had it. However, you can check on the [Ikea Website](https://www.ikea.com/) that it is still available! For further information about [the Bräda stand](https://www.ikea.com/nl/en/p/braeda-laptop-support-black-60150176/) and its story, click on the link!

![](./images/Final_Project/ikea_laptop_bräda.jpg) 

## About the Designer: Sarah Fager

According to this article about [Sarah Fager](https://about.ikea.com/en/life-at-home/how-we-work/designer-portrait-sarah-fager) that dates from the 5th of December of 2016, Sarah, who is originally from Sweden, was born in New Glasgow, Canada. Here, she learns about the IKEA store in Dartmouth through her parents; because they would always go there to buy Swedish-style furniture so they could feel at home, even though they were far away. She moved to Sweden when she was 5 years old and she pursued her career in design. Later on, she applied for the IKEA internship program.

She works at IKEA since the year 2007. She has designed a total of 200 projects for the company that is currently available in stores and is currently working on 98 prototypes.

Her first IKEA product she made was the famous [PRICKIG](https://www.ikea.com/us/en/p/prickig-microwave-lid-gray-70186090/) microwave lid. Still available today! 

The designer is also really open about the whole "IKEA machinery", basically an IKEA-tunnel in which products need to pass through, in order to make it into the store shelves and be sold. She says that 70% of her projects never make it into production. However, she understood why this selection is necessary, otherwise, the stores would have an overflow of products.

> “*They assume that IKEA is “a mass-producing company doing bad things,” but in reality “we’re a mass-producing company but we’re doing really good things.*”” - Sarah Fager.

If you want to know more about Sarah, [her instagram](https://www.instagram.com/sarah.fager.ikea.designer/) is full of updates, where you can get a glimpse of her prototypes, her work, and her daily activities!

# Part One

## 1. Function Deconstruction

In this first part we are going to analyse the project by purely looking at it. We detach ourselves from the the function of the object (which is to hold a laptop) and focus purely on the visual aspects.

### Analyze and understand the object

There are many ways to analyse an object, here I tried to analyse the laptop holder in three different ways. Since the definition of "design" is to adapt economically to everyone and everybody's needs, I figured that describing my object with the different sensory organs, we as humans have, was the best approach, because there are in fact deaf and/or blind people that might analyse an object differently than "normal" people would do. 

##### Bräda Dimension
Width: 42 cm
Depth: 31 cm
Height: 9 cm

##### Visual system (See)
Visually the object Bräda, translated to "Board" as mentioned before, is simply a board made out of plastic. What's interesting is that, when we talk about a board we usually talk about a flat board. The Bräda however, is curves 3 different times at different angles. 

###### Form of the object:
- The first curve is curved to the outside. It's the base of the object that makes contact with the desk. Here the computer is going to rest, because this curve supports the weight of the computer.
- Then the board increases slowly in high and curves to its inside. This is the highest part of the object: 9cm high.
- Until it dramatically decreases back again to its second "base". Here is last curve. Like the first one, it's curved to the outside, conversely here is a non-slip rubber that prevents the object from slipping away.

###### View from the top:
The top part is visually, the most important one, because this is going to be the part that need to be the most attractive to the consumer.

- It has a circular pattern:
     - There are two sizes of circles, the bigger ones are 3,2cm in diameter and the smaller one are 1,6cm in diameter, so basically the radius of the bigger circles.

- The pattern is also repeated:
     - A long row of the big circles parallel to the width of the object, followed by 4 smaller circles below the bigger ones. This pattern is visible on the whole top part of the computer stand.

- The pattern has a different texture:
     - Visually speaking, the circles have a more rough texture compared to the rest, which is smooth.

###### Colour:
- From a visual point of view, the laptop stand is obviously black, but this model is available in pink.

###### View from the bottom:
The bottom is the part that not many people find interesting, therefore all information and the production details are on this side. There are different elements here: 
- A rubber-like plastic is put at the base of one of the curves.
- There's two reinforcement elements, 22cm apart, at the back of the highest curve.
- Then there's a circle in the middle of the back of the board.
- Followed by the logo IKEA, the manufacture location, a bar code identification number (depends on the location of the IKEA store), the name of the product "Bräda", the type of plastic > HIPS + TPE < and finally a number that is probably associated with the date of the production year. 1 being the first models made. If they make any changes on this object (improve the quality of the material for example), the numbers might increase, going from 1 to 6. I have the 3rd model.
- The clock-like symbols below, show the year and the month of production. Companies with mass production use it to track batches internally. If a problem turns up, the date markers will allow the investigating team to run down the batch. In the photo below, you can see that the first clock is pointing out to 10, meaning 2010. The second clock point to 7, meaning the month of july. The ABCD clock might be another symbol the plastic company used to track down if a product is at default. The year and month my Bräda was made is september of 2013.

![](./images/Final_Project/Back.jpg) 

##### Somatosensory system (Touch):

Now that we got the visual part out of the way, lets get to touching the object! Touching the object allows us to understand the dimensions. This object takes a lot of surface. It's bulky, which lean it's going to take a lot of space on a desk. Its edges are round all round, even the corners. Like mentioned earlier, it's made out of hard, solid plastic.

On the back, we can see two symbols: > HIPS + TPE <

- HIPS is the abbreviation for High Impact Polystyrene. It is commonly known as the PS abbreviation, known as Polystyrene. This plastic is a low-temperature shapeless thermoplastic polymer. This plastic is able to withstand temperatures of around 120°C.
- Thermoplastic elastomer, commonly known as thermoplastic rubber, is referred to as TPE. TPE is a rubbery substance that may be manufactured using thermoplastic injection molding techniques.

Another important aspect of the Bräda is the circle-like pattern, I mentioned earlier in the "visual section". In fact, the circular pattern seemed visually unnecessary, but by touching the board I realized that the roughness was supposed to hold/grab onto something. As you put the laptop on top of the board, the rough circles are there to make sure, the laptop doesn't slide of the board!

The end result is a rigid plastic. The Bräda is made out of two mixed plastics. It feels like it's cheaply made in mass production. Over the years, this plastic might have been chemically improved by the manufacture company. However, playing with the different moulding aspects (roughness and the smoothness) might have been the most logical and smartest idea the designer came up with. Because to the general public, it's almost unnoticeable that it has another function other than visually appealing.

##### Auditory system (Hear), Olfactory system (Smell) and Gustatory system (Taste):
The reason I'm putting the hearing, smelling and tasting system together is because with the Bräda there is no really smell or tasting to it. Plastic stays plastic at the end of the day. Wether it'll be soft or hard plastic, it's not for consumption, but rather for functional purposes. 

Although the Bräda object doesn't obviously talk, it still has an interesting sound to it. Just by tapping on it, the sound you get is really prominent and recognizable as the sound of plastic. It's quite different from other materials, such as wood who absorbs more of the sound, or metal, that just like plastic reflects its sound wave back.

### The utility of the Object:
Now that we analysed the object in all its forms, this object was made to put a laptop on it. There's a whole "positioning of the body" when we sit at the desk all day long. To elevate the computer we are elevating our head as well, therefore not hinging and affecting our necks.

The only good reason I think the designer chose to work with plastic is firstly because of it's cheap manufacture process, making super affordable for everyone (4.99 €).

### The encountered problems of the Object:
The more I analysed this laptop holder the more intrigued I got with the fabrication process and the way it was designed. Multiple problems were encountered:

1. **Only one position:** 
Other than the fact, that it's purely described as a "computer stand" is quite sad. I understand it's the year of 2010 and computer/ laptops were still bulky at that time. Conversely, I don't understand why the Bräda was not aimed to also be a book (and/or a tablet) holder? It only allows one position and it's not even foldable to be stored away. The only position it offers, puts a limit on its usability, which brings me to my next point.
2. **Designed primarily to be put on a table:**
The fact that its design for a flat table, is a burden for the user. This means it's not user friendly, therefore not ergonomically. But just because a designer says so, doesn't mean the user is going to do it right? Well, wether I've tried the laptop stand on my knees while on a chair or on my lap on my bed, the Bräda object is still quite uncomfortable. 
3. **Bulky, therefore reinforcement are needed:**
Just because it's hard plastic doesn't mean it's not going to bend. In fact the amount of surface this object contains is quite impressive, to the point where it needs extra support at the back, in order for it not to bend. The 2 reinforcement elements at the back, are far enough spread one from the other (22cm) in order to guarantee stability.
4. **Non-slip rubber gets used:**
The rubber at the back, ceases to work properly over time. Throwout the years this non-slip rubber gets so used and hard, that it makes the stand slippery. The object however, still does its job nicely by holding the computer.
5. **The curved base is so high:** It feels uncomfortable typing on the computer. The most important problem encountered.

I think most of these problems come from the fact that this object is a "unique size". A product that fits in all situations is described as "one size fits all." Kinda like in the fashion industry, where one t-shirt is suppose to fit multiple sizes. Most of the time, this approach doesn't always work on designing an object, because just like humans, different objects need different things to work properly.

## 2. Solve a real problem

**Before Expert Opinion!**
Apart from the "unique size" problem, another big problem this computer stand doesn't consider, is certain ergonomically parts. For example, what really bothers me personally about this object,is the way our hand just rests on the edge of the plastic. This design doesn't consider future Wrist problems!

To solve this problem, it's important to understand the human dimension in relation to the computer itself. Because the computer stand, just like every other object in the market place, was created by someone. This person might have solved the issue for himself, but that doesn't mean this solution is going to be the right for everybody.

 In this segment, everything I describe is considered to be solely based on my posture and the way I sit in front of the laptop, but if you relate how I sit, this might apply to you too.

It feels like this Laptop Holder was designed to be put on a desk and be separately used as a second screen. Even if on the [Ikea Website - Bräda](https://www.ikea.com/nl/en/p/braeda-laptop-support-black-60150176/) the pictures show a multipurpose of the object (on knees, etc.), the different manipulation lead to problems such as the horrible hand "rest". 

I, therefore want to create a laptop stand, that is first and foremost comfortable for my body and my personal needs. Secondly, I would like something less "bulky", something transportable and retraceable. I also want something that mimics the rigidity of the plastic. Something that is not going to swing or fall over to the sides. 

## 3. Understand specific dimensions

**Before Expert Opinion!**
To sit comfortably in front of a computer, a certain posture is recommended to prevent back and/or neck pain. These element below are "standard postures" recommended by different experts. However it does not mean, that every recommended posture is "made" for everybody. Standard is not unique.

1. Always maintain a distance of 40cm to 75 cm, between the top of the screen and the eye.
2. It's recommended to have a 20° to 30° angle, between the eye level and the top of the screen.
3. The top of the screen should be at the eye level.
4. Always keep the arms parallel to the body.
5. The shoulder and the back should be straight.
6. Always keep the elbows and knees at 90° to 110°.
7. It's recommended to put some Padding in front of the keyboard. This supports the hand.
8. A slight inclination is recommended on the footrest as well.


# Part Two 
**Before Expert Opinion!**

## 4. Sketch, prototypes and error, improvements

### Sketch
One of the first ever thought I had, was a two piece that would be held together by a magnet. But I thought if I would lose one, the object would become useless. This was done before even analyzing the object.

![](./images/Final_Project/1Try.jpg)  

But after done the analyzing part, I understood that the inclination part was there to get the laptop screen as the same eye level. So I though why not put a flat part on top, to put the laptop on it? the bottom part could be used to put a further keyboard. Obviously this Idea is not coherent with my Idea of transportation.

![](./images/Final_Project/2Try.jpg) 

Finally, after seeing everything that this objects couldn't offer, I started to get to a part of the process, where I needed to see everything from a different angle. Therefore, what if I turned the object upside down? 

![](./images/Final_Project/3Try.jpg) 

Following this narrative, I proceeded to think quickly about compactness. Retraceable maybe one or the other way. Maybe I'm going to fold at the curves. 

![](./images/Final_Project/4Try.jpg) 

Here is another idea of retraceable. Below is the view of the object from it's size. I played with the different angles and hand rest. Right now, the right object below seems the most interesting so far.

![](./images/Final_Project/5Try.jpg) 

As you can see from my first sketches, I mostly played with the side view of my object, since this seemed the most interesting. While I continued sketching, I found myself looking at the object in 3D. I also started to look at it from the top and seeing the object from different perspectives, it helped me realize that the "retraceable" method I was imagining before would not work. Therefore I imagined: What if this laptop holder has the format of a A4 paper? (210 × 297 mm) This would allow me to fold this stand in 2 seconds.

![](./images/Final_Project/Rotation.jpg)

### First Prototypes (Dirty prototypes)

#### First Try
Here is my first prototype. Visually it's not appealing, but we are yet at a stage where beauty isn't required. As you can see, I mostly worked with Tesa Tape, brown cardboard and wood. 
- The Tesa Tape is a fixation method used to put every element in it's place.
- The brown cardboard represents the material I would like to work with later on.
- The wood is mostly used as the rotation system (wood cylinder) and lock system (prevent from rotation).

This prototype was essentially made, to see a rotation system around an axis (here theres two rotation). But another important part, was to see where to place, exactly the "stops", to prevent further rotation and "lock" the object in place.

![](./images/Final_Project/Sketch1.jpg)

**Below there's the different stages of unfolding the object:**
At 0. The object is folded. It's super thin, light weight, the perfect size and it makes it transportable. Ready to go. I'd like to compare this folding method as a door hinge.

**The first stage to unfold the object:**

1.Roll it from the back to the front.

2.Continue rotating it, until it can't no more. The first folding is locked.

**The second stage to unfold the object:**

3.Roll it from the back to the front, again.

4.Continue rotating it, until it can't no more. The second folding is locked.

![](./images/Final_Project/Stages_of_Folding.jpg)

This prototype was made to be used as a laptop stand, but as you can see, it supports a tablet as well. Both are supported by a strip at the base of the stand, that is flush to the base of the computer. Laptops can be really heavy, so the fact that the prototype actually stands, really makes me happy!

In case a computer gets really hot (because there are a lot of programs running on it) and it needs good air circulation, this stand makes that possible (compared to the Bräda stand), because as you can see, there is nothing below the computer and/or tablet.

![](./images/Final_Project/Prototype1.jpg)


**BREAK** **After Expert Opinion!**
After this first try, there was a lot going on my mind. I decided to rethink and understand the specific dimensions in relation to **MY** body. Although, the "rules" mentioned at [3. Understand specific dimensions](#-3.-Understand-specific-dimensions) seem to make the User put a lot of effort into its posture, the designs and invention made so far by people around the world just seem too far off, of my personal dimensions. 

As mentioned just earlier, I want to create a laptop stand, that is comfortable for my body and my personal needs. Therefore, I am focusing on the point 7 of these recommendation, because Wrist Tendonitis is a real problem I encountered many times. 

## Expert Opinion: Physiotherapist

I decided to contact a good friend of mine who happened to be a young Physiotherapist. 

He said that most patients who have a Wrist Tendonitis, most likely come from a upward tilt of the hand, because there's a lot of stress being put on the hand. I mentioned my problem being Wrist Tendonitis, so the tilted laptop stand is my main problem. 

I also said that if I happened to get rid of the tilted stand, that the eye level would not be on the same level as my laptop screen and therefore would cause neck problems. He proceeded to say that solving both problem on **a laptop** would be impossible, since the screen and keyboard are 2-in-1 objects. Both cannot be separated. 

As the name states, the portable computer “laptop” was made to stay on “top” of the “lap” of a person. With time, I understood that in the long-term, such devices would cause neck problems.

He then guided me to change the tilt in a direction I totally was unaware of. Instead, it is tilting upwards, I need to tilt the stand downwards, because the tilting had to correspond to the motion of the hand. Therefore, the “natural” way of a relaxed hand is to go down.

The following problem we got, is that the hands need to rest on a soft cushion under the wrist. The softness allows good blood circulation and no pressure point on the wrist itself.  

### Takeaway from expert opinion
The Takeaway I got from the expert, that I was unaware of, is that the tilt is a major element to Wrist Tendonitis. All in all, if there’s a tilt then there's a wrist problem, but getting rid of the tilt causes neck problems. In most cases, this problem is solvable with fixed computers at home. **However, my focus is only on the wrist here.**

The recommendation is to put some Padding in front of the keyboard. This supports the hand. 
Below are some examples: 

**A. Example: Hand Tilting with a at-home/fix computer:**

1. The hand rests on the table, but it need to tilt upwards in order to work/type properly.
2. The hand rests on a cushion and it takes a slight tilt position downwards.

![](./images/Final_Project/Fix_Computer_Tilt_Cussion.jpg) 

**B. Example: Hand Tilting with a flat laying Laptop:**

Laptops are very different from a at-home computer, because the keyboard and screen are 2-in-1, which was very innovative 25 years ago, but causes more problems than solutions.

1. The mousepad of my Laptop is so big, that the designer of this computer took advantage of the surrounding space to create a "rest" for the hand. This allows a proximity with the keyboard. However the problem is the same, since the hand is tilting upwards.
2. Here I tried to type with a wrist cushion on the table, but it was to far away from the keyboard, hence why my arm is resting on the cushion, instead of my wrist. 
3. The third position is the best, because the wrist rests on the cushion and you can see my fingers are tilted down. The only problem is this covers the mouse part.

![](./images/Final_Project/Laptop_Tilt_Cussion.jpg)


**C. Example: Positive Slope Angle: Hand Tilting with a upwards tilted Laptop:**

Let's take the Bräda Laptop stand again for demonstration:

1. Here is the problem that occurs with a tilted laptop stand.
2. Even with a cushion, the risk of Wrist Tendonitis is still very high, because the hand is not relaxed.

![](./images/Final_Project/Hand_Tilting_Tilted_Laptop.jpg)

**D. Example: Negative Slope Angle: Hand Tilting with a tilted Laptop:**

Bräda Laptop stand another demonstration:

The solution the expert proposed to me is to turn the tilt in another direction. I need to tilt the stand downwards, because the tilting had to correspond to the motion of the hand. Therefore, the “natural” way of a relaxed hand is to go down.

In order to so, other problems occurred: 

**On a desk:**
I still need to keep my elbows at a 90° angle in order for my hand to tilt down naturally. This means I need to be in a chair that is adjustable in height. That being said, the legs need to rest on a foot step as well!

**On my lap:**
Again: 90° angle at the elbows!

1. The hard object

![](./images/Final_Project/Hand_TiltingDown_Laptop.jpg)

### Conclusion

In conclusion, the Ikea Laptop stand is, ironically advertised to me used in a way that is totally unusable in the long term. In fact, it causes medical problems, and it's funny that the designer talks about IKEA as a mass-production company, that filters 70% of her projects, when ironically she doesn't do her job correctly. 

It seems I'm being hard, but now I have the knowledge. In fact, this object was released around the year 2010. This object has not been improved in its design whatsoever. The material might have changed, but the shape stayed the same for the past 10 years. It makes me mad that the designer or IKEA have not asked for "consumer experience", but at the end of the day it doesn't matter for them, because selling it for cheap, bring money in.

So, in the position I am today and after getting to the bottom of this object's problem, I continued my research about wrist problems among people who use keyboards on a daily basis. I was led to a study conducted in 2004, by the *Journal of Orthopaedic & Sports Physical Therapy* of the Marquette University located in Milwaukee, Wisconsin. It studied 314 employees.
The [Design Features of Alternative Computer Keyboards: A Review of Experimental Data](https://www.jospt.org/doi/pdf/10.2519/jospt.2004.34.10.638?fbclid=IwAR1wXRbr__zyV-CRo4kvBMEAINCoIINZZCoBy-uVW4bP8oniJXGBFYkResM) study was conducted by professor named Guy G. Simoneau of the department of Physical Therapy, and an associate professor Richard W. Marklin from the department of Mechanical and Industrial Engineering. They mention that different keyboard design elements are designed to reduce pain and the risk of musculoskeletal diseases that have been linked to long-term usage of traditional computer keyboards.

To put things into perspective, the study was made 6 years prior to the release of the Bräda Stand! This gave the designer enough time to make her own research.

## Study: Positive/Negative Slope Angle

Previously I talked about a Positive/Negative Slope Angle, but I wasn't going into the details of the different slope angles that are necessary to allow a comfortable position of the hands/ wrists on the keyboard.

In the study, 314 people were put to the test. They, not only tested different angled keyboards, but they also analysed the slope of the hand in relation to the Keyboard. 
* Having the keyboard at a 0° angle would elevate the wrist angle to 15° upwards.
* Tilting the keyboard upwards to 15°, would elevate the wrist slope to 21.7°
* Tilting the keyboard downwards to -15°, would decrease the wrist slope to 8.8°.

**Results:** A significantly wrist extension angle (12.9° difference) is noted in a 30° keyboard angle slope!
Not only does a downwards tilted keyboard, put less flexion on the wrists itself, it also puts more flexion on the fingers! Making typing more comfortable! 

**When using a keyboard with a negative slope of 7.5° and a wrist rest that slopes with the keyboard, wrist extension can be lowered to nearly 0°!**

![](./images/Final_Project/WristSlope_Study.jpg)

Interestingly, the studies also mention the different height of the wrists and the keyboard in comparison to the elbows. This is also something the Expert (Physiotherapist) explained to me. Drawing from the expert:

![](./images/Final_Project/Kine.jpg)

* By working on a 15° tilted keyboard and the wrist was positioned 4 cm below the elbows, an extreme wrist extension angle of more than 27° was observed. <This is bad.>
* By working on a -15° keyboard and the wrist was positioned 5 cm above the elbows, a less wrist extension angle of less than 2° was measured. <This is better.>

Below is the chart of the average measurements taken of the 314 people:

![](./images/Final_Project/WristSlope_Study_Table.jpg)

It's interesting to note that within 10 minutes, people adapted really fast to the experiment, mor precisely to the alternative keyboard features. They could type almost the same speed and accuracy as with a standard keyboard. 


#### What is the best tilt for my hand?

For the next few prototypes, I'd like to put in action the different types of inclinations the study proposes. I'd like to create the perfect angle for my wrist.

I prototyped 3 angles: 
* 20° slope, because it represents the angle of the Bräda stand.
* 15° and 7.5°, because I took the slopes the study proposed.

![](./images/Final_Project/Wrist_Angles.jpg)

##### Test 1: 20° slope

This is the slope of the Bräda object. For me this angle is really bad and painful for me, because it's to high. I can't see using this for typing for a long period of time.
* Left: wrist and elbows are at the same level. 
* Right: Wrists are 4cm below elbow. This causes extreme wrist extension.

![](./images/Final_Project/Test_20.jpg)

##### Test 2: 15° slope

The 15° slope is bearable for a short period, however, still not adequate for a person like me with wrist tendonitis. The expert, clearly said to avoid wrist extension as much as possible.
* Left: wrist and elbows are at the same level.
* Right: Wrists are 4cm below elbow. This causes medium wrist extension.

![](./images/Final_Project/Test_15.jpg)

##### Test 3: 7.5° slope

Seems more comfortable (2 to 3cm between wrist and elbows), but goes again against expert guidelines.
* Left: wrist and elbows are at the same level.
* Right: Wrists are 4cm below elbow. This causes slight wrist extension.

![](./images/Final_Project/Test_7,5.jpg)

##### Test 4: -20° slope

After testing the -20° slope, I came to the conclusion that this angle is only good when I'm standing up. Otherwise it's super uncomfortable on the desk and on the lap! Here you can see the Bräda Stand on a table that is 95cm height! I'm 1m55. 

As you can see, this angle belongs to the category of negative slopes, just like the expert said. However, this angle wasn't made for me. My wrist can only flex (down) to a certain limit, after that is becomes painful, just like it is in extension (upwards). 

![](./images/Final_Project/Test_-20.jpg)

![](./images/Final_Project/Angles.jpg)

##### Test 5: -15° slope

This is where I start to get comfortable! My wrist was positioned 4 cm above my elbows.

![](./images/Final_Project/Test_-15.jpg)

##### Test 6: -7.5° slope

At a 7,5 negative slope I find myself to be in the best position! My wrist was positioned 2 to 3cm above my elbows. 

![](./images/Final_Project/Test_-7,5.jpg)

 As you might see later, I'm not going to be tilting my computer the 'traditional' side anymore. I am going to be lifting the keyboard upwards and keep the screen down.

## First Prototypes (Dirty prototypes)

Now that I know the perfect tilt for my wrists, the only thing missing was knowing where exactly I put my hand on my computer. To do so I simply did an analysis of my hand on the laptop, by using a stamping method: I painted the bottom of my hand and stamped it on a paper layer, traced of my laptop. As you can see, the form it takes is a triangle one. Based on this given information, I would create the next prototype.

**Attention:** I did the stamping based on the hand I use the most, because I am right-handed! If you are left-handed, stamp your left hand! This is important, because it's on the most used hand where tendonitis tend to occur! 

![](./images/Final_Project/Wrist_Stamp.jpg)

#### Second Try: First Prototypes

In this second try I created a prototype made out of cardboard. The idea about this design is to insert a element on the edge of the laptop. 

This object is going to be made out of two elements! I figured, after testing the different positions of the laptop on my lap and on my desk, it really didn't matter if it's only one object or two. However, I liked the idea of having two objects, because that would mean I could adapt those for different kind of body means/ measurements. 

![](./images/Final_Project/Project_Form.jpg)

Here you can see this prototype is made out of different layers.


Another thing the expert said, that the hand needed to rest on something soft, like a cushion. To allow a good air circulation. Conversely, the design I did here is based on the position of my hand/ wrist in relation to my computer! Here there is no cushion, but the idea is to have a convex form that is going to 'hug' the wrist. The goal is so that my wrist 'relaxes' on top of this object.

![](./images/Final_Project/Project_Form_Prototype.jpg)


## Second Prototypes (Clean prototypes)

I already knew how I wanted to do this laptop stand. It was important to me to diffuse this information to anyone around the world, equipped with a 3D machine (and with the same wrist problems as I have) to reproduce this object and potentially better the position they are in. Therefore, I decided to build this object from scratch using the Fusion 360 program, as seen on the module 3!

#### Top Part:
In this first clean prototype, I only reproduced the top part on where the wrist is going to rest. The print setting I used on this prototype are: 0,30mm DRAFT mode. I used a white PLA filament. There are no supports, since I rotated the object to minimize any lost material. And I did an infill of 20%, since there would only be a 20minute difference from 20% to 5%. I finally sliced the object and exported it to gcode. I also preheated the bed to 70°C to prevent lifting. Everything is explained in more details in Module 3 .

* I started off by creating a sketch of a rectangle that measures 90mm by 80mm. I also created an extension of this rectangle: 5mm. This is going to be on the outside of the laptop.
* Then I measured the roundness of the angle using the Fillet tool in Fusion 360. I rounded it by 10mm.
* After exiting the sketch mode, I proceeded to extrude the whole sketch 1cm upwards.
* On the bottom of the sketch, the extension made earlier of 5mm needs to be extruded downwards of 10mm, like the top part.

![](./images/Final_Project/Prototype2Fusion.jpg)

Here is the first try of the object:

![](./images/Final_Project/Prototype2.jpg)

Here is the object in action on the edge of the laptop. As you can see the roundness of the angle is flush to the edge! **Note that this is only the right side of the laptop stand!**

![](./images/Final_Project/Prototype2_Laptop.jpg)

#### Top Part Rounded:

I redid the same top part but this time I corrected the roundness on each corner. I retook the Fillet Tool and entered the same measurements: 10mm for each corner.
Below you can see how the edges are not pointy anymore. 

![](./images/Final_Project/Round_Edges.jpg)

The interesting part about this object is that it can be used in different positions in relation to the laptop. On a desk for example, the object can be placed top of the laptop, as well as on the back (if neck pain occurs). Or if ever you want an slight elevation of the laptop, you can put the piece on the under side!

#### Bottom Part:

Now I needed to create an extension that would incorporate the different inclination we talked about earlier! As I mentioned the best wrist flexion for me is in between 0° to -7.5° to -15°! Past these slopes; like the -20° of the Bräda stand is just to much for me to use on the desk and on the lap. The only time I felt comfortable with the -20° slope was while I was working on a 95cm (950mm) table while standing. Other than that the -20° slope is to much of a tilt for my wrist.

This white prototype is basically in between the 0° and the -7,5°. It will keep the wrist 10mm higher above the keyboard. The problem is that it's not "fixed" to the edge of the laptop and can fall anytime. In conclusion, this prototype is a base for the wrist.

For the tilt I had two sloping methods:

A) The first being: Working with "Extensions" of different hights to the object. 
I know that for 7,5° tilt i needed 25mm in height. (1st extension)
And for the 15° slope I needed 45mm in height. (2nd extension)
With all three elements together, the object looks like this:

![](./images/Final_Project/Assembled.jpg)

B) The second method would be to use a material that elevates the laptop. Here I used the previous object to demonstrate my thoughts.

![](./images/Final_Project/Objet_2.jpg)

## 5. Adaptation, distribution, edition

### Pré-Jury

At this stage of the project, there are still little things I needed to be aware of and that were explained to me during the pré-jury, that was held on the 16th of december 2021. All in all, this day might have been one of the most interesting, yet exhausting and intense days of the semester. The day was organized into 5 jury groups, each made out of 2 to 3 people, meaning I had to present my project 5 times. Although, there could've been a better organization, repeating myself multiple times during the day, helped me dive deeper into unknown details. Every time the juror had no clue what my project was about, but everyone had different valid opinions!

In global, my project was really well appreciated, especially my analysis on the ergonomics of a hand wrist. This only motivated me even further, to continue this journey! Although the positive feedback, a few details still needed to be addressed:

* A possibly adaptation to other laptops, such as Windows, Macbook, Thinkpad, etc.
* The comfort of the object itself: 
     * Does it need padding, if so what kind and where? 
     *  How about the imprint/ relief of my hand on the plastic? 
* The transportability of the laptop stand.

### A) Adaptation to other laptops

At the moment, the laptop stand is only customized for my laptop (Apple Macbook Pro) only. To make this a worldly distributed product, it was necessary to implement an adaptation to multiple laptops. Therefore, as most company's do, I looked out for the most common laptops with 13" in 2021 and I narrowed it down to:

* Apple's Macbook Pro (13 inches, The one I have) and Macbook Air (13 inches)
* Windows's HP Envy x360
* Windows's Dell xps 13
* Windows's Lenovo ThinkPad X1
* And finally Windows's Lenovo Yoga 9i

**Photos of all computers:**

| Photo |  Name    |  Dimensions |           Link           | 
|-----|-----------------|---------|--------------------------|
| ![](./images/Final_Project/Type_Computer/Macbook_Pro.jpg) | Apple's Macbook Pro   | Length:304,1mm / Width:212,4mm / Height:212,4mm / Thickness:14,9mm | [Link: Macbook Pro](https://support.apple.com/kb/SP754?locale=en_GB) |
| ![](./images/Final_Project/Type_Computer/Macbook_Air.jpg) | Apple's Macbook Air   | Length:312mm / Width:232mm / Height:232mm / Thickness:13-23mm | [Link: Macbook Air](https://newatlas.com/2016-macbook-comparison/46938/) |
|![](./images/Final_Project/Type_Computer/HPEnvyX360.jpg)| Windows's HP Envy x360 | Length:306,5cm/ Width:194,6mm/  Height: 194,6mm/ Thickness:16,4mm| [Link: HP Envy x360](https://www.hp.com/my-en/shop/hp-envy-x360-convert-13-bd0507tu-bundle-4e3y1pa.html)|
|![](./images/Final_Project/Type_Computer/dell_xps13.jpg)| Windows's Dell xps 13   | Length:295.7mm / Width:198.7mm / Height:198.7mm / Thickness:14.8mm | [Link: Dell xps 13](https://www.dell.com/en-us/shop/dell-laptops/xps-13-laptop/spd/xps-13-9310-laptop/xn9310cto210h)|
| ![](./images/Final_Project/Type_Computer/Lenovo_ThinkPadX1.jpg)| Windows's Lenovo ThinkPad X1  | Length:323mm / Width:217mm / Height:217mm / Thickness:14.95mm | [Link: Lenovo ThinkPad X1](https://www.lenovo.com/us/en/p/laptops/thinkpad/thinkpadx1/x1-carbon-gen-8-/22tp2x1x1c8?orgRef=https%253A%252F%252Fwww.google.com%252F)|
| ![](./images/Final_Project/Type_Computer/Lenovo_Yoga9i.jpg) | Windows's Lenovo Yoga 9i | Length:318,4mm / Width:210,9mm / Height:210,9mm / Thickness:17,6mm | [Link: Lenovo Yoga 9i](https://www.lenovo.com/us/en/yoga/?orgRef=https%253A%252F%252Fl.facebook.com%252F&fbclid=IwAR3muJoAIzuhjTa91wsQg2leXsT_nLVIdIMv9n7a3mYsx--kTnO2E1CsZ2o) |


I came to the conclusion that these more modernized laptops, all adapted a bigger mousepad and therefore a big margin between the edge of the computer and where the keyboard actually starts. As I talked to a good friend of mine, who is into the Tech Business, majority of people tent to chose a 13" laptop, since it's remarkably cheaper (around 1.600 to maximum 2000€), compared with 15 to 17 inch laptops that go for around 3000€ to 4000€.

#### First Try in Fusion 360 and 3D Print

As a conclusion, I corrected the previous modeling object in Fusion 360. The first try to adapt my laptopstand to different computers, had to be well thought out.

1) I started of by eliminating the rounded part of the interior edge, since only an Macbook would perfectly fit into that corner, after all I made it exactly for my laptop.
![](./images/Final_Project/JuryFinal/CorrectCorners.jpg)
2) Based on the information of the different thicknesses of laptops (see table of laptops above), a clip would be a perfect fit, because of it's flexibility and adaptability to different keyboard thicknesses. I then started to analyse different ways to get this object to "clip" into different computers. Below the computer a "pin" keeps the laptop tilted and secondly prevents the laptop from slipping away. Although I had the idea to propose 3 slopes of different angles (7,5° and 15° slope) to the user, making one unique object (one size fits all) was tough to integrate multiple slopes in. Therefore, in this part we will focus on the adaptation first and if I'll manage to integrate different slopes later, I will.
3) Continuing, I got inspired form a hair pin, also known as a "Bobby Pin". The way this pin is designed, straight on one side and wavy on the other, allows the hair to get "pinched" so it doesn't move.
Therefore, this form will only be visible at a side view of the laptop: The <span style="color: orange;">Orange Form</span> is adaptable for multiple computers and allows a good grip since et the <span style="color: lightblue;">bottom and middle part</span> are made out of non-slip plastic. 
![](./images/Final_Project/JuryFinal/BobbyPin.jpg)

Integrating this technique into the Fusion 360 program, I had to work on the flexibility of the material itself. I continued on the model I previously had and adapted a clip system into it. In photos 1 to 4 I am on Sketch Mode, playing with different circles. Then trimming the unnecessary parts and finally putting everything on offset. Notice how one part is thicker (5mm), than the edge (1mm)? I got inspired form the first exercise I did on the "Module 2: Autodesk Fusion 360". The thinner the more flexible. 

Photo 5 is the object and Photo 6 is a thought on how to insert a potential padding. But I'll talk a little more about "Comfort" below.

![](./images/Final_Project/Projet_Fusion360/ESSAY1LAPTOP.jpg)

![](./images/Final_Project/Projet_Fusion360/Try1.jpg)

The initial 2 try's on printing, the bed was preheated to 70°C, however it might had some oil, which led to a disaster that was stopped immediately. I was afraid, the 1mm would lift of the bed, but thankfully, I solved the mistake once before on module 2 and this time, everything went smoothly!

![](./images/Final_Project/Projet_Fusion360/ESSAY1LAPTOP_Print.jpg)

Finally this piece performs smoothly on my laptop. And just to put into perspective, how it performs on 2 other laptops form friends and family: **Note these laptops are not mentioned on the list below, since they are not the most "common ones"!**

Here is the result of the clip method on my computer Macbook Pro:

![](./images/Final_Project/JuryFinal/Essay1_Macbook.jpg)

<div style="height: 0; padding-bottom: calc(50.00%); position:relative; width: 100%;"><iframe allow="autoplay; gyroscope;" allowfullscreen height="100%" referrerpolicy="strict-origin" src="https://www.kapwing.com/e/61ec6e0b9c206c00d294523f" style="border:0; height:100%; left:0; overflow:hidden; position:absolute; top:0; width:100%" title="Embedded content made on Kapwing" width="100%"></iframe></div><p style="font-size: 12px; text-align: center;">Content made on <a href="https://www.kapwing.com/" target="_blank" rel="noopener noreferrer">Kapwing</a></p>

Here is the result of the clip method on a family member's Razer computer:

![](./images/Final_Project/JuryFinal/Essay1_Razer.jpg)

<div style="height: 0; padding-bottom: calc(50.00%); position:relative; width: 100%;"><iframe allow="autoplay; gyroscope;" allowfullscreen height="100%" referrerpolicy="strict-origin" src="https://www.kapwing.com/e/61ec6f4294ca47005317c92b" style="border:0; height:100%; left:0; overflow:hidden; position:absolute; top:0; width:100%" title="Embedded content made on Kapwing" width="100%"></iframe></div><p style="font-size: 12px; text-align: center;">Content made on <a href="https://www.kapwing.com/" target="_blank" rel="noopener noreferrer">Kapwing</a></p>

As you can see, the thinness of the S-shape at the bottom, allows a advantageous flexibility to the stand. Nevertheless, one thing I didn't think about was that the top part would lift of the computer. As someones who writes a lot on the Keyboard, I think such thing would actually drive me crazy especially since it always bounces back up right after I lift my hand/ wrists up. I think this problem is due to the fact that I didn't thought about the inclination. 

![](./images/Final_Project/JuryFinal/Tilt.jpg)

As I saw this, I wanted to go back into Fusion 360 and edit the model, but with further thoughts a non slip material would make it work. By inserting a non-slip material of 2mm, in between the laptop and the PLA plastic, I prevented the bouncing of the computer stand. The weight of the computer also keeps the object in place. 

I also thought it would be a good idea to put the anti-slip rug at the bottom. In global, the rug might have solved most of my problems!

![](./images/Final_Project/JuryFinal/Anti_Slip.jpg)

<div style="height: 0; padding-bottom: calc(50.00%); position:relative; width: 100%;"><iframe allow="autoplay; gyroscope;" allowfullscreen height="100%" referrerpolicy="strict-origin" src="https://www.kapwing.com/e/61ed754137a00d006bc87f87" style="border:0; height:100%; left:0; overflow:hidden; position:absolute; top:0; width:100%" title="Embedded content made on Kapwing" width="100%"></iframe></div><p style="font-size: 12px; text-align: center;">Content made on <a href="https://www.kapwing.com/" target="_blank" rel="noopener noreferrer">Kapwing</a></p>

**How to mount this laptop clip into the laptop (mackbook and windows)?**

Below, you'll see the result of the clip method on my colleges and friends computers:

As you see, the object adapts well to the 15 inch Macbook Pro. For most of the Windows Laptops it went well too. As a matter of fact, only 1 laptop out of 5 were to thick and therefore made it impossible to use the clip.

However there is a trick to slide the clip into the computer! Although for Apple PC's the clip slides well from both sides: From front and/or from the sides. But if you have a window, that is a little thicker, you'll need to slide it from the sides! Remember to always try before giving up!

![](./images/Final_Project/JuryFinal/Computers.jpg)

### B) Different Surfaces: Comfort Check

Since the beginning I wanted to make this laptop stand an open source for everybody around the world. So it was important to me to chose a material, everybody has access to. Besides my strong opinion on making this a 3D printed object only, I had to think about the fact that this object, was mostly designed for people like me who have tendonitis, therefore making comfort the number one priority. The slope aspect is already integrated and well thought out, but what about the surface of the object itself? What could I do more to offer a comfortable laptopstand? This was one of the topics talked about during my pré jury.

Here we are diving more deeper into the material aspect of the finished product. There are different ways of getting a well rested hand wrist and although padding is the best option, just like the Physiotherapist expert suggested, I wanted to explore other ways to ensure a comfortable stand. 


#### Hand Relief

This method really makes the object visually interesting, it is not so adaptable for everybody. As I mentioned above, I took a print of the position of my hand, which led me to the final "rounded" form of the object. By going beyond the "triangle shape", it would be interesting to dig out all the "excess material", on the places where my hand actually gets in contact with the surface of the object. Below, you can see the different point of contact.

![](./images/Final_Project/Wrist_Stamp.jpg)

As I got to try it out on Fusion 360, I ran into a problem. Since I had no clue Fusion had this tool, I had to do some research. I came across this Youtube Video: [Fusion360 - Tools 2 - Edit Form/Modify - Sculpt Environment](https://www.youtube.com/watch?v=XGBfN7dayNM)

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/XGBfN7dayNM?start=263" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I then noticed that Fusion 360 would let me edit my model with the FORM Tool. So I researched again: 
* [Sculpt workspace only showing greyed out models that can't be edited](https://forums.autodesk.com/t5/fusion-360-design-validate/sculpt-workspace-only-showing-greyed-out-models-that-can-t-be/td-p/7290727)

* [Turning Off Caption Design History in Autodesk Fusion 360](https://www.engineering.com/tutorials/turning-off-caption-design-history-in-autodesk-fusion-360/#:~:text=To%20turn%20the%20feature%20off,in%20the%20file%20is%20gone.)

These websites helped me a lot. All in all, I had to do a few things in order to work with the FORM tool.

1) First turn off the Design History of you model and press "CONTINUE".
![](./images/Final_Project/JuryFinal/DesignHistory.jpg)

2) This allows you to go to Form Tool on the Top Bar. Now I had select the object, right click and press "CONVERT". By converting I was able to edit the top surface lenght and with spaces. I entered 10 on each side.
![](./images/Final_Project/JuryFinal/DesignHistory2.jpg)
3) Now the only thing to do was to create a mold of my hands into the surface. And although I put some effort into making it work, Fusion 360 didn't seem to work like another program I'm aware of called Maya, by Autodesk. As you can see I had trouble getting 
![](./images/Final_Project/JuryFinal/DesignHistory3.jpg)


As you can see I struggled to make this method work on Fusion 360. As I have no materialistic results, I think I might know why this method would not work. As this design is systematically molded to my petite hand, it makes this design personal. Therefore it's not adaptable for everybody, such as someone with big hands.

If I wanted to print this as a whole object, the material would be so hard and uncomfortable to even work on. Imagine it like this: Our hands have already padding integrated, which makes a flat surface feel more comfortable than on a surface with a relief. This is because on a flat surface, the weight is distributed evenly, where as the Relief "pokes" the palm on specific points, making it super agonizing.

You remember the Bräda object that had the edge holding the computer? This imprinted mold of my hand would feel like this, if it were another person using my hand stand. It's just not possible to adapt this for everybody.

#### Sponge Padding

After the test of the Hand relief on the surface of the Computer stand, I came to the conclusion that a sponge would be best on this case. But there are different kind of sponges. Just like mattresses, some sponges are more "dense" and some are more "spongier". Although, they all bounce back to their "normal" position, the <span style="color: pink;">pink sponge</span> had more holes, making it more lighter in material (not weight!), therefore making it perfect for a Hand wrist to rest on. 

The only thing to be aware of is the thickness of the <span style="color: pink;">pink sponge</span> since this also plays a big role on comfort. Two layers of this sponge, will most likely "hug" the wrist, giving the user a comfortable laptop stand. 

![](./images/Final_Project/JuryFinal/Sponge_Padding.jpg)

Here I'll be trying to print the 3D object and implemented the sponge into it. According to the expert (Physiotherapist), this is the best option to guarantee a comfortable hand/ wrist rest. The sponge can be implemented different ways on top of the sponge. Here for example I saved the roundness of the plastic and simple extruded a space for the sponge. I also kept in mind the anti-slip section.

Here is the 3D printed part: 

![](./images/Final_Project/JuryFinal/Sponge_Padding_Object.jpg)

Here is the sponge on top of the model: It's really comfortable! Although I would just eliminate the edge and put the sponge on the total of the surface.

![](./images/Final_Project/JuryFinal/Sponge_Padding_Object2.jpg)

#### Flex PLA

Here I'll be showing you a test I made with different types of PLA plastic. To guarentee a comfort, there is a PLA Plastic called FLEX PLA. As the name states, this plastic compared to the regular PLA is a lot more flexible. Altought there are different types of Flex plastic (ultra soft, medium soft etc), I wanted to try it out. Keep in mind that this material is a lot more delicate since it requires a lot more patience with the heating temperature of the nozzle and the bed.

First I thought how to change the plastic in the middle of a 3D print. Is this even possible? And yes! There are different ways to change the plastic during a print. 

**A) With the first method**, I can tell the printer where exactly to stop. Here I decided to change the plastic at height 38mm, therefore the 3D machine is going to stop at precisely this height, in order for me to chang the plastic to a FLEX one.

Below in the photo, you can two different colors: 
* The Orange color represents the <span style="color: orange;">PLA Plastic</span>.
* The Red color represents the <span style="color: red;">PLA FLEX Plastic</span>.

![](./images/Final_Project/JuryFinal/PLA_Change.jpg)

Whilst this method is advantageous in many ways, it's not the most logic. As you can see this requires a lot of useless support, therefore making it wasteful.

**B) The second method** is a lot more simpler and doesn't require a change during a print. It's basically cutting the **.stl** file at desired hight (here I chose 37mm) and then perform cut and export each part individually into a **.gcode** file. 

In total, there is one gcode file, that is going to be printed with the FLEX PLA plastic. A second gcode file with the left laptop stand and a third with the right laptop stand, both will be printed with normal PLA plastic (1.75mm).

This method allows a quick simple print of multiple files plus it saves material.

![](./images/Final_Project/JuryFinal/PerformCut.jpg)

![](./images/Final_Project/JuryFinal/gcode.jpg)

Results: Although I visioned a successful result, it's not always the case. In all I ran into a problem. In the FabLab we didn't have any FLEX PLA left and despite my efforts to buy some FLEX PLA or even let it print somewhere, it seemed like this plastic somehow disappeared at the moment I needed it most.

As mentioned earlier, there are different types of FLEX and since I already had an example with a cushion, I figured FLEX PLA would do the same thing. I had to change my tactics. I went back to the prototype where I rounded the edges and I thought it would be good to try different typed of textures, that guarantee a better grip.

#### Textured Surfaces for better Grip

Until now, I have been saving on material by always printing the whole model on it's side. Just as I wanted to print on one part of the object with FLEX PLA, did I notice that I could perform a cut in Prusa Slicer. This discovery led me to explore the Prusa Slicer program even more.

As I talked to one of my professors, on the problems I encountered with the FLEX plastic, he advised me that just because a soft surface is related to comfortable, doesn't always mean that hard one is related to discomfort. He said, the problem isn't the hard plastic, it's the way I present it, most importantly how I decide to 3D print. See, I've been so focused on saving the material that I forgot that the comfortable aspect had already been uncovered: the -7,5° slope.

Consecutively, printing the object on it's side, leads to layers of plastic that are going to help the hand glide on it. Notice how the strokes are all parallel and pointing towards the screen.

![](./images/Final_Project/JuryFinal/Texture_Surface.jpg)

Textured surface on the other hand, help the hand to stay on the surface. The idea behind "grip" in Design is the notion of different textures on top of certain objects, that are there not only for the aesthetic, but to guarantee a grip of something. Just like the Bräda Board, the circles were there to "grip" the laptop, so it doesn't slide easily of the board, when a little inclination is present.

* Notice the <span style="color: blue;">surface texture</span> is printed differently? It's still parallel stokes, but they are sideways, which means the hand doesn't glide. Perpendicular lines would do the work too.
* The <span style="color: red;">surface texture</span> was done differently. In fact, as I got to research how to implement different types of textures into a 3D model, I came across the [Prusa Printer Forum's website](https://forum.prusaprinters.org/forum/original-prusa-i3-mk3s-mk3-others-archive/example-of-different-pei-bed-textures/), that talked about a specific Bed Sheet sold by Prusa. As you can see this sheet is a lot more rougher than the "normal smooth". This textured sheet gives the object a certain look and I wanted to see how it would look on the object's surface. As much as I wanted this to work, this sheet somehow cased more harm than good and after two prints I quickly abandoned the idea. I was used to the old plate and this plate just seemed to make my model lift up from the bed.

![](./images/Final_Project/JuryFinal/Texture_Surface1.jpg)

Although, my urge to figure this problem out, I continued to explore the Prusa Slicer Program and as you guessed it, there is a way I wan print different textures with the nozzle. No need for bed sheets. The <span style="color: blue;">blue surface texture</span> was already done this way (Top Fill Pattern: Rectilinear and/or Monotonic), but I wasn't aware I could choose the design. To change the top pattern in you're Prusa Slicer, go to PRINT SETTING > INFILL (on the left) > Select the desired TOP FILL PATTERN.  

![](./images/Final_Project/JuryFinal/Top_Fill_Pattern.jpg)

Below you'll see 3 textures I chose that could potentially help with the grip:

1) Concentric: Division of 3 parts, where each section goes to different direction.
2) Archimedean Chords: Consecutive line that resembles a spiral.
3) Hilbert Curve: Consecutive line that resembles a greek pattern.

![](./images/Final_Project/JuryFinal/Texture_Surface2.jpg)

### C) Transportability, Distribution and Edition

#### Transportability

After all the thoughts put into the laptop holder, the question I needed to ask now was: How are people even going to transport such object? Well to do so, I propose a little hand-made pouch. Although I have no knowledge on how to sew, I had a friend of mine, Sarah who was kind and helped me. I also checked out this websites [How to make a drawstring bag](https://www.chicaandjo.com/how-to-make-a-drawstring-bag/) and I followed this video [DIY drawstring pouches, easy sew.](https://www.youtube.com/watch?v=jylJvQMKRLI) 

<iframe width="560" height="315" src="https://www.youtube.com/embed/jylJvQMKRLI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![](./images/Final_Project/JuryFinal/Transport.jpg)

To have the laptop stand on the go, put it into a little pouch and off you go!

#### Distribution

The Final project is a one size fits all, with a unique design for people like me who suffer from tendonitis. Making it a distribution to everybody and every FabLab around the world.

Although for the pré-Jury I had a design with multiple slopes (7,5° and 15°), I found it extremely difficult to implement that design to multiple laptops. Therefore, I opted for the 7,5° slope, which as mentioned above, is personally the best position. Although I am a quite petite person, my wrist are positioned 2 to 3cm above my elbows. 

Nevertheless, I managed to get a parameter into the Fusion 360 program. Anyone who wants to download this file and implement it on their personal laptop, only has to measure the height (distance between the edge of the laptop until the start of the keyboard) and the width (distance between the edge of the laptop until the start of the mousepad.) 
For the MacBook Pro I used <span style="color: orange;">90mm length</span> / <span style="color: blue;">85mm width</span>.

![](./images/Final_Project/JuryFinal/Parametres.jpg)

People will then only need to insert their measurements in the Fusion 360 program. Like I explained on Module 2: Autodesk Fusion 360, to find the Parameters go to: 
* MODIFY > CHANGER PARAMETER > USER PARAMETER. Then click on the + sign and enter the desired value.

Finally the only thing left to do, is to export their Fusion 360 into a **.stl** file and transform and slice it into a **.gcode** file in Prusa Slicer. For more information, please follow the instructions on module 3: 3D Print.

Although the idea behind the parameters was to make this design personal, I ran into a problem in Fushion 360. Basically as I corrected my form, I always created a copy, which was not related to the first sketch in the first place. Therefore, as I got to create my parameters later on I ran into the problem that I had created to many sketches which made me confused and in the short amount of time we had, it was impossible to solve this problem. But it is possible.

![](./images/Final_Project/JuryFinal/Parametres_Table.jpg)

![](./images/Final_Project/JuryFinal/Parametres_Table2.jpg)

There is however a method on sizing the object to the personal need: By putting the desired length and with in the Prusa SLICER in the right bottom. See the stl file below! 

![](./images/Final_Project/JuryFinal/Parametres_Table3.jpg)

#### Edition

This Object went through so many processes. Sometimes I thought it got to a point, where it might work and sometimes I got lost on my tracks. At the end I helped myself, by documenting every single thing. Swinging back and forth trough my modules kept my mind fresh throughout the process.  

Despite the fact that the laptop stand got through so many changes, I think this model kept it's position from the start and for that reason I'll call this the first edition: 1.1 

## 6. Name and Pictogram

I named this laptop stand TILT!

![](./images/Final_Project/JuryFinal/FINAL.jpg)

![](./images/Final_Project/JuryFinal/FINAL2.jpg)

![](./images/Final_Project/JuryFinal/FINAL3.jpg)

## 7. Future Improvements:

Potetially have a multi functionnal padding, that can be 3D printed. Maybe have a magnetic system in between the padding and the clip. 

<iframe width="560" height="315" src="https://www.youtube.com/embed/0rkTgPt3M4k?start=774" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Useful Documents :

#### Document FUSION 360

https://a360.co/3nXFEcI

#### Documents: gcode

1) [LEFT_BOTTOM_LAPTOP_4h20m.gcode](./stl_documents/FINAL_PROJET_DOC/LEFT_BOTTOM_LAPTOP_0.2mm_PLA_MK3S_4h20m.gcode)
2) [RIGHT_BOTTOM_LAPTOP_4h20m.gcode](./stl_documents/FINAL_PROJET_DOC/RIGHT_BOTTOM_LAPTOP_0.2mm_PLA_MK3S_4h20m.gcode)
3) [LEFT_TOP_LAPTOP_1h38m .gcode](./stl_documents/FINAL_PROJET_DOC/LEFT_TOP_LAPTOP_0.2mm_PLA_MK3S_1h38m.gcode)
4) [RIGHT_TOP_LAPTOP_1h38m .gcode](./stl_documents/FINAL_PROJET_DOC/RIGHT_TOP_LAPTOP_0.2mm_PLA_MK3S_1h8m.gcode)

#### Documents: svg files for Lazer Cut EPILOG` & stl

1) [Anti Slip .svg](./stl_documents/FINAL_PROJET_DOC/Test1_EPILOG_AntiSlip.svg)
2) [Strip Anti Slip .svg](./stl_documents/FINAL_PROJET_DOC/Test2_EPILOG_AntiSlip_Strips_.svg)
3) [Laptop Stand.stl](./stl_documents/FINAL_PROJET_DOC/Essay3_MultiLAPTOP_RoundEdgesv4.stl)