## Hi there!

This is just a little introduction about me, myself and my work!

## About me
It's so nice to meet you! 

I'm Joana, but you can call me Jo. I'm currently in the first year of my Masters’s Degree in Brussels at the Faculty of Architecture La Cambre Horta. 
Here you can see me on one of my recent trips to Greece, one of my favorite countries. This photo was taken in Chania, at the old harbor, which interestingly was influenced by Venetian architecture! As you can see I like to explore places I've never been to before!

![](./images/Previous_Work/ME.jpg)


## My Background

I'm proud to be Portuguese. However, the country that gave me the most opportunities since I was a child was Luxembourg. I've always been interested in Arts. 

Since young, I always found new projects that could entertain me. It wasn't until high school at "L'école des Arts et Métiers" where I fully developed a more profound theoretical thinking of how things are the way they are. Learning how to draw correctly might have been the biggest challenge for me. It's easy to "love to draw" when there are no critics afterward. Just like when a child shows a painting, parents aren't going to judge. 

But once you start to grow older, you understand that drawing is not only for passionate people but also for people who want to go deeper into more meaningful thinking besides physical drawing.

After graduating with a diploma in Arts, I decided to pursue my career in architecture, because I had realized that art and architecture, throughout history, were always somehow connected and balanced each other. Now that I had all this knowledge in Arts, I wanted to see what Architecture could offer.


## Previous work

Believe it or not, architecture wasn't always my first option! I always dreamt of doing some graphic work, such as a graphic designer or product designer. The first programs I worked with were Illustrator (Adobe) and MAYA (Autodesk). Here are some of the early works I did while still in High School.

### Graphic Work and 3D work
This logo I made back in 2017, with Adobe Illustrator. It was for a seafood restaurant. We then had to create a whole visual Identity: Menu, Cupholders, Stickers, Napkins, etc.

![](./images/Previous_Work/Logo2.jpg)	


### 3D
The two pictures shown above are the few of the projects I did by using Maya. It helped me get creative with the program and understand the different aspects of lighting a project and how the consumer sees the product (Renders). 

![](./images/Previous_Work/3D_Models.jpg)	


Right now, into my Master's degree, I got the opportunity to enroll in a course called "architecture and design" and I'm super excited to take this learning opportunity, to dive deep into the world of FabLab.

## My object:
The object I chose to analyze and potentially correct and/or modify its design is a Laptop stand. It’s called Bräda, from the Swedish company IKEA.

![](./images/Index/ikea_laptop_bräda.JPG)

## Contact

Feel free to contact me:
joana.fernandes.monteiro@ulb.be

