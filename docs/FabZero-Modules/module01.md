# 1. GitLab

## Changing to a directory

This week I worked on defining my final project idea and started to get used to the documentation process.
I started by doing a listing of the current directory files, by inserting the command "ls" in my terminal.

```
Joanas-MacBook-Pro:~ joanafernandes$ ls
Adlm			Downloads		Public
Applications		Library			Zotero
Creative Cloud Files	Movies			bin
Desktop			Music			pymel.log
Documents		Pictures			xgen
```

Once I figured my directory to change to, I changed it to "Desktop" by using the command "cd", also known as "changing directory".
```
Joanas-MacBook-Pro:~ joanafernandes$ cd Desktop
```
To make sure I was in the Desktop Directory I used the command "pwd", "print working directory".
```
Joanas-MBP:Desktop joanafernandes$ pwd
/Users/joanafernandes/Desktop
```

## Git install / configuration

The exercise of today's "MODULE 1" was to get to know how Git works. To familiarise myself with the program, we were asked to install it on our computers. Since I have a macOS, the installation was quite easy. First I googled "How to install Git on macOS?" The first [link](https://git-scm.com/book/fr/v2/Démarrage-rapide-Installation-de-Git) showed me how to follow different steps on how to install it.

Once installed, I followed the [guide](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git) given by our professor step by step. I configured my username to "joana.fernandesmonteiro" and my user email to "joana.fernandes.monteiro@ulb.be"

```
Joanas-MacBook-Pro:Desktop joanafernandes$ git config --global user.name "joana.fernandesmonteiro"
Joanas-MacBook-Pro:Desktop joanafernandes$ git config --global user.email "joana.fernandes.monteiro@ulb.be"
Joanas-MacBook-Pro:Desktop joanafernandes$ git config --global --list
user.email=joana.fernandes.monteiro@ulb.be
user.name=joana.fernandesmonteiro
```
With the command "git config --global --list" I made sure that my username and email were configured correctly.

## Set up SSH keys to connect your local computer to the remote server (such as gitlab.com) in a secured way.

The following steps, also given by the professor in this [guide](https://docs.gitlab.com/ee/ssh/index.html).
The "ssh-keygen -t ed25519 -C "joana.fernandes.monteiro@ulb.be"" command uses the encryption ed25519, which asked me for a password to generate the public and private keys.

```
Joanas-MacBook-Pro:Desktop joanafernandes$ ssh-keygen -t ed25519 -C "joana.fernandes.monteiro@ulb.be"
Generating public/private ed25519 key pair.
Enter file in which to save the key (/Users/joanafernandes/.ssh/id_ed25519): 
```

Since I didn't have a ssh directory, the command generated it automatically for me:
```
Created directory '/Users/joanafernandes/.ssh'.
```

Here we can see it asking me for a password :
```
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
```

It also asked me the location to put the private and public key. In my case, I put it at a default location "/Users/joanafernandes/.ssh/".
```
Your identification has been saved in /Users/joanafernandes/.ssh/id_ed25519.
Your public key has been saved in /Users/joanafernandes/.ssh/id_ed25519.pub.
The key fingerprint is:
SHA256:UZSWBa92xb9regA0mcx+ErqYZaog99IXIY1i0TujZMQ joana.fernandes.monteiro@ulb.be
The key's randomart image is:
+--[ED25519 256]--+
|  . .    .*=+    |
|   E .   .+X .   |
|  . . + ..+ + o  |
|   = * o = = o . |
|  + o + S + =   .|
| . +   = o . .  .|
|  o + . .     .. |
|   . + .       o.|
|    . .      .+. |
+----[SHA256]-----+
```

## Setup GitLab with SSH key

[Here](https://docs.gitlab.com/ee/user/project/deploy_keys/index.html), I followed the guide on how to deploy my ssh key onto Gitlab. 
First, I used the command "cat", "concatenate and print files" to print out my public key onto my terminal.

```
Joanas-MacBook-Pro:Desktop joanafernandes$ cat ~/.ssh/id_ed25519.pub
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKPq1X1gub84EYuLIyFjhISawogU8uZMOSHW+9kqTsLj joana.fernandes.monteiro@ulb.be
```
I copied my public key "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKPq1X1gub84EYuLIyFjhISawogU8uZMOSHW+9kqTsLj joana.fernandes.monteiro@ulb.be", into my Gitlab SSH keys tab.

![](../images/Module_1/Setup_GitLab_with_SSHkey.png)

## Git Clone with SSH

Finally to clone my private repository, I went to Gitlab on my repository and copied the "clone with SSH" text into my terminal.

![](../images/Module_1/Git_Clone_with_SSH.png)

I proceeded to use the command "git clone <copied text>".
```
Joanas-MacBook-Pro:Desktop joanafernandes$ git clone git@gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/joana.fernandes.monteiro.git
Cloning into 'joana.fernandes.monteiro'...
The authenticity of host 'gitlab.com (172.65.251.78)' can't be established.
ECDSA key fingerprint is SHA256:HbW3g8zUjNSksFbqTiUWPWg2Bq1x8xdGUrliXFzSnUw.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added 'gitlab.com,172.65.251.78' (ECDSA) to the list of known hosts.
Enter passphrase for key '/Users/joanafernandes/.ssh/id_ed25519': 
remote: Enumerating objects: 42, done.
remote: Counting objects: 100% (42/42), done.
remote: Compressing objects: 100% (38/38), done.
remote: Total 42 (delta 19), reused 0 (delta 0), pack-reused 0
Receiving objects: 100% (42/42), 206.55 KiB | 669.00 KiB/s, done.
Resolving deltas: 100% (19/19), done.
```

Then I confirmed the download of the repository with the command ls.
```
Joanas-MacBook-Pro:Desktop joanafernandes$ ls
joana.fernandes.monteiro
```
I proceeded to change into my downloaded repository by using the "cd" command. 

## Git add / Git commit / Git push

Firstly I checked the status of the modified files.

![](../images/Module_1/git_status.png)

Secondly I need to stage my modified files, by using the command git add. 

![](../images/Module_1/git_add.png)

I verified that I staged my file with the command "git status".

To commit the file, I "git commit" as a command and added "Rajouté mon nom dans README.md" as my commit message. I also verified by using git status.

![](../images/Module_1/git_commit.png)

Finally to push my changes into my private Gitlab repository, I used the command git push.

![](../images/Module_1/git_push.png)


From this point on, I can work at ease on the documents that I pulled from GitLab. This will not affect the GitLab server since I’m working from my computer. I can edit as much as I want to and only when I feel ready to commit and push, only then will everything be available on my website!

I am ready to document!

## Usefull Websites:

1. [Git Configuration](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git)
2. [Download Visual Studio Code](https://code.visualstudio.com/download)
This will help you see your webside at he the same time you are writting! 
3. [How to change Markdown Theme](https://mkdocs.github.io/mkdocs-bootswatch/?fbclid=IwAR2cojBDtkeUeoi47RgVidXlxqEEEgz7H4wbAiy_CHaIIV-mO5pMfJLkgG4)
Here you can chose different themes for your website. In the folder "mkdocs.yml" go to "theme" and change the name. My website is called "minty".