# 3. 3D Print

This week I worked on a 3D machine. We got to see our models come to life!

## Fusion 360 Recap

On the first hour of the day, we focused on doing a little recapitulation of the program we learned the week before: Fusion 360. As I explained in the "Module 2" folder, Fusion 360 was pretty much easy to use. I had forgotten to mention a little detail while documenting, which I have now fixed: The parameter of the model. To understand this please go here to [Module_2](./module02.md) and check under "Fusion 360 - Second Exercise - Personal Object".

## Prusa Machine: Presentation
Prusa is the name of the 3D machine we worked on today. Its name comes from its founder Joseph Prusa and what's interesting about this machine, is that even some pieces of the machine are 3D printed themselves. Fortunately, we already have one at home, so it isn't the first time I use a 3D printer. When we first got the Prusa there was a whole manual on how to build the machine. Yes, it took us more than 24h to build it, but with the help of gummi bears, we got there. We have quite a bit of fun at home!

Here is a little description of the machine:
![](../images/Module_3/PrusaMachine.jpg)
 

## PrusaSlicer: Download and Installation

To install the Prusa program also called Prusa Slicer, go to Prusa's [Main Page](http://www.prusa3d.com/drivers). Since there are different types of printing machines (Mini, MK3S, SL1, MK3, etc. ) I selected the "ORIGINAL PRUSA I3 MK3S & MK3S+", because this one is compatible with the ones we have at the FabLab. You'll also need to download the program, depending on the computer you have (Linux, Apple, Windows). Since I have a Macbook, I downloaded it for macOS. 

After the completed download, install the program on your computer.

![](../images/Module_3/Installer_Prusaslicer.png) 

## PrusaSlicer: How to use it
As you can see in the photo below, Prusa Slicer is a program that shows you exactly the plate of the 3D machine. On the plate is the name of the type of 3D machine. It may be, as it was in my case, that you downloaded the MK3 version, but on the plate is written "Mini". If that's the case, you'll need to change the printer, depending on the type of printer you have.

Just go to PRINTER > click on the gear > Add a physical printer. As a window pops up, change or add the desired printer. 

For the print settings select: 0.20mm. 

And for the Filament, we used "Prusament PLA", because this is the type of plastic we use in the FabLab.

You can see on the right top a selection between "Simple", "Advanced" and "Expert". Select the Expert mode, as there are more options and useful information.

> ATTENTION: Prusa Slicer is a program that only sets up your model ready to print. As you can see in the photo below, there are no buttons on the left and on top that edit your model. To create/edit a model you'll need to use Fusion 360 or another 3D modeling program. 

> Imagine it as a working sheet: you first need to create your document on a writing program, such as Word or OpenOffice. You then need to export it into "PDF" and only then can you print (ctrl+P). The way you chose to print a document, is the same way you chose to print a 3D model, the only exception is that it's 3 dimensional and on a plate.


![](../images/Module_3/Presentation_Prusaslicer.png) 

## Editing my Fusion 360 Model

As there is no way to edit my model on Prusa Slicer, I had to go back to my Fusion 360 document where my model was. Last week, on the [Module_2](./module02.md), the last step I made on the "Second Exercise - Personal Object", was to rotate my object so it was facing the right way, as a laptop holder should stand. 

However, I realized quickly this week, that if I wanted to 3D print my model, that this step didn't need to be taken. It’s easier for the Prusa Printer to print my model sideways. This way I have a cleaner model and there is no waste on unnecessary plastic because there would be no support holding the model together. It's a win-win situation.

So I finally changed my model back to its side!

### Export Fusion 360 into .stl

As I compared beforehand the 3D printing with a normal A4 printer at home, I talked about exporting a Word document into "PDF". "STL" is essentially the "PDF" of the 3D printing world. Of course, there are other exporting ways, but the most used is STL. 

As we worked on Fusion 360 last week, everything got saved on the Fusion 360 server. This week we had to export our model into an "STL" file.
With my model edited and ready for printing I went to FILE > EXPORT > Choose the file or USB stick.

## Transform .stl into .gcode in Prusa Slicer

With the Prusa Slicer program open, set up, and ready to go, I dragged my STL file into the Prusa Slicer program. Now that it's in there, the model may be over-dimensional, and that it surpasses the plate. You'll need to reduce its dimension by going to SCALE FACTOR in "OBJECT MANIPULATION", on the right side of the program.

I started with X=10; Y=10; Z=10, but it gave me an error saying the object wasn't there anymore. So I figured I needed to play a little with the dimensions to see the object on the plate. It's also a matter of logical thinking, right? The 3D machine might be able to print little things, but when something is really small, the printer is going to have difficulty printing, therefore it wasn't able to recognize my model.

So by changing the Scale factor into X=30; Y=30; Z=30, my model became visible and centred on the plate. It did not require additional editing such as moving, etc.

The main goal of today's exercise was to be able to print our 3D model in less than 2 hours. As you can see on the right bottom corner, the estimated printing time on the normal (and stealth) mode is 1 hour and 17 minutes.

I think it comes truly due to the fact, that I edited my model sideways; therefore, reducing time on printing unnecessary supports. 

All there is left to do is click "SLICE NOW" on the bottom right corner and "EXPORT G-CODE".


![](../images/Module_3/DragObject.png)

## Ready to 3D Print

> Disclaimer 1: Clean the bed of the printer, since we don't want any oily particles on it. Clean it with acetone or isopropyl alcohol. These products are going to strip the oil easily.

> Disclaimer 2: Check your environment. If there is an open window near the Prusa Printer, close it. Dust may come in and alter your model. Small actions like these make beautiful models.

There are different types of ways you can send your .gcode to the 3D printer. There is the WiFi way, the Raspberry pi way, or for beginners the SD Card way. For the sake of everybody, we chose the SD card to insert our gcode. 

By inserting the SD card at the left side of the 3D printer (see photo below). The only thing left to do is select the file. I named mine "Joana.gcode". 

> Remember that, when you select your file, the printer isn't going to print right away! It needs to calibrate and the plate (or the bed as they call it) needs to be warmed up. 

On the screen you can see on the top left: 

* The "nozzle" temperature = 213/215°C. The bed temperature = 60/60°C. 
* SD is at 1%. This only means how far along the printer is. So the 1% means, it just started printing.

Then at the top right:
* Z means at what height the printer is. Again 0.80mm, means it's at the beginning of printing.

![](../images/Module_3/PrusaPrint.jpg)

### Encountered Problems

Right about when everything seemed to be going great, there was a small problem that occurred during the print. 

This could be due to different things:
* Either the object is too small and thin
* Or it could've been due to me cleaning the plate/ bed wrongly.

Either way, these problems didn't seem to be logical because I made sure I cleaned the bed properly. So why would the object print good on one side, but start to lift on the other side? The size and thinness weren't making any sense.

![](../images/Module_3/PrintProblem1.jpg)

### Problem... But still printing?

With the problem I encountered, I would either decide to stop this prototype and redo the whole process of cleaning, etc. But I decided to see where the printer would essentially go with it. 

My main concern was that it would completely detach from the bed. That's why I stayed and watched the process. 

It seemed at first, as the more height it was getting, the more it would be detached. Because logically thinking, my model is just a big triangle, and the more it kept printing the more strength it was putting on the top of the model. So, if one side is lifted and the other not, depending on where the nozzle is putting its strength, it could potentially lift the whole thing.

It's important to mention as well, that it worked because my shape is essentially simple. The 3D machine continued printing because it was a repetitive process. Had it been a different type of shape, where it requires support, it’ll continue printing but everything would be a mess. 


![](../images/Module_3/PrintProblem2.jpg)

Surprisingly, the printer seemed to have adapted to the lifted part and continued printing, as if nothing happened. I think it might have just played with the different x, y, z-axis. 

It’s just amazing how it kept printing straight and the bottom part just seemed deformed!


![](../images/Module_3/PrintProblem3.jpg)


## Final Product:

As you can see, my model seems good. You can still see some imperfection on the top left side, but for the rest, it's really good as a first try!

Of course, it's supposed to be a laptop holder, but I put an SD card on it so you can see the dimension it has.

![](../images/Module_3/FinishModel.jpg)

## Final Group Photo:
![](../images/Module_3/GroupModel.jpg)

## Solution to the Problems
I must say that the problem I encountered, made me dig more into the understanding of why it happened the way it did. And I must say it just seems logical: The bed wasn't warm enough!

In my previous work (Red Model), the bed was at 60°C which means, some parts of my model were attached better than the other. From an analytical point of view, just seeing where is the most "material" (plastic) on my model, is the part where it didn't lift. The end of the model is less material, which means it got "colder" faster, therefore detaching from the plate. See photo below:

![](../images/Module_3/MyModel.jpg)

The solution was to redo the model by heating the bed a little bit more. The bedding temperature on the first model (Red Model) was set at 60°C. This time I added 10°C more, making it 70°C.

![](../images/Module_3/Essay_2_Temp.jpg)

To change the temperature:
* Click on the Prusa main button (while it is starting to preheat), scroll to "TUNE" > click on "Bed" > Chose the desired temperature.

I also changed the SCALE FACTOR to X=40; Y=40; Z=40, just to see what the model would look like if it was bigger. And as the results show there is no lifting!

![](../images/Module_3/DoingGreat.jpg)
![](../images/Module_3/NoLift.jpg)

However, I kept the challenge to print the model under 2 hours. This model took 1 hour and 45 minutes to make.

## Results
The new grey model has an imperfection, due to some oiling issue of the printer rodes. This happened due to some skip of a few micro millimeters.

![](../images/Module_3/Final_2.jpg)

## Usefull Websites and Documents :
1. [Prusa Slicer Download](http://www.prusa3d.com/drivers)
2. [Prusa Main Page](https://www.prusa3d.com/)
3. [Computer Holder.slt](../stl_documents/Module_3/STL_Computer_Holder.stl)
4. [First Try .gcode](../stl_documents/Module_3/Joana.gcode)
5. [Second Try .gcode](../stl_documents/Module_3/Essay_2.gcode)