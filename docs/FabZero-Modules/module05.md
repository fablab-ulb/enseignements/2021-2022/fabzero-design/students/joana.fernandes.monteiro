# 5. Machine assisted by computer

In FabLab there are two wood cutting/ carving machines. There are two Shaper Origins and one bigger CNC machine.
In this module, we got to learn how to use a Shaper Origin machine. Although this machine does a very similar job as a basic CNC wood carving machine, it varies in size.

## Safety Information

##### Basic Information:

- Always use the machine on a flat & stable surface. Make sure the table is not wobbly.
- Always use a vacuum dust collector.
- Always click the Red Button (so the bit can rise) if not in use or if you'd like to cut another shape.
- To cut a wooden plate for example, "lock" it onto the flat surface with double-sided tape. The tape also allows certain carved shapes to stay in place, since they are "glued" into place.

##### Other Information:

- Keep your body in balance by keeping your feet apart, to better control the Shaper while it's moving. 
- Make sure the Shaper is not at the edge of the table, so it doesn't fall.
- When it comes to adequate clothes and/or hair, jewelry, etc., make sure they don't interfere with the carving process. Accidents happen very fast. Better to prevent than regret.


## Shaper Origin Machine
![](../images/Module_5/Shaper1.jpg)

![](../images/Module_5/Shaper2.jpg)

![](../images/Module_5/Vacuum.jpg)

Here is the Vacuum we have at FabLab. This must be turned on each time you want to cut something.

## Shaper Information

##### The machine itself:

- Weight 6,6 kg
- Height 298,5 mm
- Depth 196,9mm
- Width 349,3mm

##### Information about the carving:

- Maximum cutting depth: 43 mm (Z Axis)
- Collet diameter (inside the lock): 8 mm
- Supported file format: SVG (Vector Images)

## Shaper Origin: 

#### What is it?

The Shaper Origin is a machine, that cuts different elements, with the help of a carving bit. There are different sizes of bits as well. It is usually manually guided by a person.

![](../images/Module_5/1.jpg)

#### What is it for?
What's good about this CNC machine is its size, making it everywhere portable. It usually comes in handy on a construction site.

#### How does it work?
The shaper works based on a given digital file (SVG). Like the Lasersaur and the Epilog, machines like this tend to prefer vectorial files, made in programs like Illustrator and Inkscape.

#### Prepare your working surface
Today we are carving a wooden plate of around a thickness of 4mm. First, "lock" the wooden plate onto the flat surface with double-sided tape. 

![](../images/Module_5/3.jpg)

#### First machine adjustments: Shaper Tape
To cut into a wooden plate the shaper needs to digitalize its surroundings first in order to recognize the flat surface it wants to work on. It does that with the help of a visual mark, called "Shaper Tape". This tape is nonetheless a tape with various dots on it, looking like the domino game. 

![](../images/Module_5/2.jpg)

Normally the Shaper machine needs around 4 "dominos" to be on each stripe (of the Shaper tape) on the flat surface. And depending on the work surface you need, you might need more Shaper tape

##### Place the Shaper Tape correctly

Remember the Tape should be in a position, in front of the Shaper for it to scan. You can put as much tape as you like, as long as it's in front. 

If there is not enough shaper tape or no tape at all, the machine won't be able to scan and recognize its surface.

If the machine doesn't recognize enough tape, you can go back by clicking on "ADD TO SCAN" > "ADD A SHAPER TAPE" > then place your tape and scan the surface again, until the machine recognizes the surface.

![](../images/Module_5/Tape.jpg)

##### Human-machine interaction
While it’s carving into the wooden plate, there is a two-way exchange happening. While the person is "guided" by the machine by looking at the digital drawing on the touch screen, the machine is "guided" by the person by being pushed and slid around. Interestingly, the machine seems to understand and tolerate a few little mistakes made by the person who guides it. If there is a big slip made by the person, the machine does not tolerate it and immediately stops the carving process.

For technical details, such as the speed of the carving bits must be manually input by the person itself. A machine does not know what type of speed or size you'd like.

##### Changing Carving bits
First, make sure the machine is off the electric current. You will need to unplug the middle part, the metal cylinder (see below).

Then, you need to use a Key that comes with the Shaper Origin machine. This T-shaped key (see below) loosens the whole middle part of the Shaper machine. Take it out. 

To change a carving bit, hold the button on the side (red) to lock the rotation system. Then with a wrench loosen the collet (blue part) and change the bit. After the desired bit, redo the same steps and place the metal cylinder into its place. Don't forget to re-plug it! 

![](../images/Module_5/Carving_Bit.jpg)

##### Insert file in the Shaper Machine:
Like the Epilog and Lasersaur machine, the Shaper Origin opens its files with the help of a USB Key, provided by the company as well. 

After making sure that the forms/ shapes are grouped, not made from segments of lines, and after saving the vector file into an SVG file on a computer, it's time to save the file into the USB key. Insert the key into the computer. Click on the touch screen of the Shaper IMPORT > CHOOSE DOCUMENT.

Here you can choose the centre of the Form (white dot), rotate and you can edit the dimensions.

## Different types of cuts:
- Inside a shape
- Outside a shape
- Exactly on the line of the shape
- Carve the shape (pocket)

![](../images/Module_5/5.jpg)

By default, the Shaper machine wants to cut "on line". If you want to choose its position to "inside" for example, click on the left top side of the touch screen on "on line" ("sur ligne" in french) > the click on "inside" ("intérieur" in french). Each time you finish carving out a shape, the type cut must be re-selected again.

It can be annoying when there are a lot of shapes to carve out, but unfortunately, it's a default setting set by the Shaper itself.

![](../images/Module_5/Cuts.jpg)

## Different types of speeds:
There are 6 types of speed settings o the Shaper Origin machine. See the "Carving Bit Speed Control" Button on the description image of the machine. Each speed has its own advantage and disadvantage.

![](../images/Module_5/7.jpg)

- Number 1 seems an accurate speed to begin with, but here for instance it's too slow. The result above shows us not so crisp and clean edges.
- Number 6 is the maximum speed. For beginners, this is too fast, and results show burned edges because the carving bit was moved too slow. With the 6th speed, there can be an overflow of dust and smoke as well.

Remember that being fast doesn't always mean good results. A carving bit moving at the fastest speed requires a lot of control of the person guiding the machine. 

## Different passes and types of offsets:
A pass means how many times a bit passed a line. Here you can see that one single pass is not enough to make the edge smooth. More passes are required, depending on how much time you have and what the exercise is. For a prototype, for example, one pass is enough. 

Offsets on the other hand, (see below) are prototypes of different joints. For example, if you want to create a wooden chair/table, you'll need to find a system that is going to "put together" the legs of the chair and its body. Offsets are just an example of different joints.

![](../images/Module_5/6.jpg)

## Finished Results:
After guiding the machine, click on the red button to make the bit stop spinning. Don't forget to turn the button to OFF. Also, turn off the dust vacuum.

![](../images/Module_5/4.jpg)

## Useful Websites

- [Shaper Origin GitLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Shaper.md?fbclid=IwAR3YfQHFz7t0VP8Em3iT0C5DJ2s4WE2_OKRHSKgsMNZBmE3THjI83OetA2g)
- [Shaper Data Sheet](https://www.shapertools.com/fr-fr/origin/spec)
- [Cut Speeds](https://www.youtube.com/watch?v=MEk43U8mf7g)
- [Passes](https://www.youtube.com/watch?v=qxoSaDLJRTY)
- [Offests](https://www.youtube.com/watch?v=sLo0yUkkO3M)

