# 2. Autodesk Fusion 360

After a tough week on getting to know GitLab, I must say I'm starting to get the hang of the documentation process. I'm a new beginner, so remember everything is possible!

On the other hand, I want to talk about what I did this week! I learned a new program called Fusion 360. Strangely, but thankfully it wasn't that hard to get the hang of it. It was easier than getting to know GitLab. So let out by me explaining how I installed the program.


## How to install Fusion 360

By clicking on this [link](https://www.autodesk.com/products/fusion-360/personal), you will get forwarded to the Autodesk website, where you can download the free Fusion 360 version, for personal or hobby use.

At this stage, you will have to create yourself an Autodesk account, but if you are just like me and already have one, just log in with your email. 
You then need to complete some additional data, by filling out your first and last name, country, email, etc. By now you should be able to click next and download the program.


## Fusion 360 - Guide

We had the honour to meet Thibaut Baes, who was nice and patient with us. Thibaut presented his 3D printed Chocolate work, which sounded so complex and simple (and delicious) at the same time.

He proceeded to explain different tools, workflows, and working environments, such as the "sketch" environments, that subsequently allowed us to understand how Fusion 360 works. So, what's the best way to understand a new program? By practicing and manipulating it.


![](../images/Module_2/Fusion360_1.png)

> On this photo you can see that under the "SOLID" tab, there are different kinds of sub-tabs, such as the "CREATE" tab, the "MODIFY" tab, the "ASSEMBLE" tab, the "CONSTRUCT" tab, and the last tree "INSPECT", "INSERT", "SELECT" tabs. 

> On the bottom left side is a timeline, that saves the different steps it takes to make the 3D model. If anything happens along with the processes of modelling, it is always possible to "go back in time" and edit a missing step. One thing I particularly found useful with this feature, is that once something is edited, it doesn't delete the "future" work, which is something I'm used to. Adobe for instance has a "history" of a project, but once gone back in time and edited, all future work is gone.

> As for the middle features, those are mainly for rotation reasons. We have the "ORBIT" tab for a 360° rotation, but also the "HAND" and the "ZOOM" feature that allows us to move or zoom in or out.


## Fusion 360 - First Exercise - FabLab Logo

### 2D to 3D - Rectangle to cube:
In the sketch-mode under the "SOLID" tab, I started by creating a centred rectangle 100mm/100mm wide. In this case, it's a square.

![](../images/Module_2/1_rectangle_cube.png)

I proceeded to extrude (e) this square from off both sides, by selecting a symmetric Distance. The measurement for the whole length is the same as the dimension of the square: 100mm. This step could have been done in two steps: by extruding once from the top by 50mm and from the bottom by 50mm, adding up to 100mm in total. It just shows that simpler options are possible and available with Fusion 360.

![](../images/Module_2/2_Extrude.png)

### Curved Edges:
With the "Fillet" (f) feature, it's possible to round up the edges of an object. The roundness is personalized by 10mm. It expands so far so that a cube can become a sphere.

> ATTENTION!!!
This step was made at the very end of this model. I inserted it here to show, that it's always possible to edit a model by using the Timeline, as I explained earlier. I recommend however to do this step at the very end, as the instructions may vary. The next photos shown below are already with curved edges, but the instruction follows as if there are no curved edges. Please keep that in mind, as I don't want any confusion
!!!


![](../images/Module_2/3_CurvedEdges.png)

### Extruding first cylinder in cube:
Now that we have our main cube finished, it's time to extrude the inside of the cube. By going into the Front Side of the cube, we select "Centre Diameter Circle" (c) and give it a value of 50mm.

![](../images/Module_2/4_Circle.png)

It's time to extrude (e) the circle to the other facet of the cube, either by dragging it or by inserting the value of -100mm, since we already know that this cube measures 100mm/100mm/100mm.

![](../images/Module_2/5_ExtrudeCircle.png)

### Extruding second cylinder in cube, by rotating the first one horizontaly:
For the second cylinder, there is no need to repeat the same process. 

We can simply select the first (already extruded) cylinder, then go to CREATE > PATTERN > CIRCULAR PATTERN. And since we want it facing the other direction, we rotate the cylinder 90°. This makes the job easier.

![](../images/Module_2/6_CreatePattern_CircularPattern.png)

![](../images/Module_2/RotateCylindre.png)

### Extruding third cylinder in cube, by rotating the first one vertically:
Repeat same steps: Select first cylinder.

Then: CREATE > PATTERN > CIRCULAR PATTERN.

In CIRCULAR PATTERN: 
Choose RED Axis;
Rotate: 180° (in total Angle);
Choose quantity: 3

![](../images/Module_2/7_RotateCylindreVertical.png)

### Extruding first side:
For this part, we must change the camera to the front side of the cube again.
I started by creating a random rectangle (the size will be adjusted right after), starting at the edge of the square and dragging it down to the middle of the green axis. At this stage the curved edges are visible, however, this can be ignored as we concentrate on the middle square with the cylinder already carved in.

As the rectangle is fully random, we will need to select the RED line (just as shown above) AND the middle point. Finally, click on CONSTRAINTS > MidPoint. That way both align in the middle. 

Here the selected distance is 25mm. And you can finalize by extruding to the back.


![](../images/Module_2/8_ExtrudeSidePart.png)

### Extruding second and third side:
At this point, the second and third sides can be extruded too, by inserting an Axis. To do this, leave the sketch area first then go to CONSTRUCT > AXIS THROUGH TWO POINTS.
Then choose two points of the cube you want the axis to go through. Just like the photo:
> Remember: The edges are visibly round here. However, the moment I made this model, I worked with a plain cube. The round edges were the very last step!


![](../images/Module_2/9_Axis.png)


Still outside the sketching area, continue by going to CREATE > PATTERN > CIRCULAR PATTERN.

Select the extruded part, by clicking on the 2 faces, in "Objects".
The axis we want is the one we just created: Select the blue Axis in "Axis".
Then proceed to select quantity 3.

By clicking "OK" Fusion 360 should be able to extrude the last step automatically.


![](../images/Module_2/10_AxisRotate.png)

> At this point I finalized  the FabLab Cube, by rounding the edges. See [here](#curved-edges)

## FINAL PRODUCT:
For the final product, I just thought it was super cool to explore the rendering section of Fusion 360. But it’s not all done, I still have to explore more. 

![](../images/Module_2/FabLabCube.png)


## Fusion 360 - Second Exercise - Personal Object 

The object I decided to analyse and redesign this year is a laptop holder. To understand how this object works, I had to redo the object in Fusion 360, without any modification to it. The laptop holder you see here is the object at its finished stage. It is made from plastic, and it can only be used on one side, the most important side to hold the laptop.

### Parameters
To start, I measured the different distances, heights, and angles. I also had to create a parameter for my object. Creating a parameter only means (in case you want to change your object) your object is going to adapt to a certain measure, also called a parameter. Say for example my laptop holder has a certain high (or inclination) that I feel is suitable ergonomically. In this case, I would like to keep the high, so I create a parametric by going to
MODIFY > CHANGER PARAMETER > USER PARAMETER. Then click on the + sign and enter the desired value.

To begin with, I decided to work from the TOP view, as this was much better to manipulate.


![](../images/Module_2/Myproject/one.png)

When I finished with the sketch, I extruded (e) it to the top. I measured the length (42cm) and inserted 420mm.

![](../images/Module_2/Myproject/two.png)

Finally, to get my object to face the right way, I selected it and clicked on move (m). The little window "MOVE/COPY" shows up > Choose in "Move Type" the "Rotate" option.

In the Axis and depending to which side you want your object to turn to, choose the Axis that suits you the best. For me, I had to choose the RED Axis.


![](../images/Module_2/Myproject/twoRotate.png)

And Voila!

# Final product:
<iframe src="https://myulb181.autodesk360.com/shares/public/SH35dfcQT936092f0e436359979c7cc4c08d?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>


Here is the the first model I did. The FabLab Model:
<iframe src="https://myulb181.autodesk360.com/shares/public/SH35dfcQT936092f0e4377d7972b9134eaf2?mode=embed" width="640" height="480" allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"  frameborder="0"></iframe>

[How to insert your 3D Model into your website](https://www.youtube.com/watch?v=lWDh7XZASoA) 

1. Tap [A360](https://a360.autodesk.com/) on Google
2. Sign in with your Autodesk Account
3. Now you are on your AUtodesk Drive. Select "Fusion 360". This will take you to [Student Admin Project](https://myulb181.autodesk360.com/g/projects/D20211005452472351/data/dXJuOmFkc2sud2lwcHJvZDpmcy5mb2xkZXI6Y28uZVFxQjNiZTdTZG1oN2dDNUlxeDhEQQ)
4. Chose your file and share it. You can share by coping the link, by email or you can embed it. We want to embed it into our Website!
5. Select the desired size: 640x480 or 800x600 or 1024x768

[Laptop Holder](https://a360.co/3kywHF1)

[FabLab Cube](https://a360.co/3HdHSwx)

## Usefull Websites and Documents :

1. [How to install Fusion 360](https://www.autodesk.com/products/fusion-360/personal)
2. [How to Set Up User Parameters in Fusion 360](https://www.youtube.com/watch?v=iUUFrkdgCLI) Skip to minute 8:40
3. [Download Laptop Holder.stl](../stl_documents/Module_2/Computer_Holder.stl)
4. [Download FabLab Cube.stl](../stl_documents/Module_2/FabLab_Cube.stl)

