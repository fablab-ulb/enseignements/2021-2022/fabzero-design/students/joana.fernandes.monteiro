# 4. Computer Laser Cutting Machines

## Part 1:
This week we got to explore the world of Laser Cutting Machines!
Computer-assisted laser machines are simple transmitted drawings to a laser cutting machine that will then cut it, engrave it, or mark it on the material of our choice.

### Safety Information

For this first part of the morning, we got a presentation from Axel, who explained to us how to prevent potential fire hazards that can occur during a Laser Cut. Especially, when there's no one around constantly checking!

He also mentioned the different types of health hazards! For example: To not look straight into the laser while the machine is cutting! Like the sun, people can get blind when looking into something that is so bright. 
Another thing is the smoke: Depending on the different materials you are cutting into, there can potentially be a lot of smoke in the machine. So, opening a Laser Cut Machine right after it has finished cutting isn't a good idea. So please, even if you are excited (as I was), allow the smoke to get filtered first, you just must hang on for a few minutes. Also, a good tip is to allow good air circulation in the room! So go ahead and open some windows! This is not like the sensitive 3D printing Machine we saw last week!

So please in the future, keep this information in mind, as it may save your life!

### What is a Laser Cut?
Laser cutting is a non-contact cutting process that uses a small but ultra-focused laser. This laser is going to cut right through different types and desired materials. It is essentially thermal based, meaning it uses a lot of heat in a tiny focus point.


### The Materials
As mentioned just above, laser cutting can be used on different materials. However, not all are suitable to be "laser-cut" and some are even prohibited because they are highly flammable and/or toxic for the environment and the human body.


##### Recommended:
* Plywood (plywood/multiplex)
* Acrylic (PMMA/Plexiglass) - The Plexiglas brand work great.
* Cardboard and/or Paper
* Textiles 

##### Not Recommended:
* MDF: Its smoke is too thick & harmful in the long term
* ABS, PS: Melts to easily away & harmful smoke as well.
* Thick polyethylene such as PE, PET, PP: Melts to easily away as well
* Fiber-based composition materials: Very harmful Dust
* Metals: Impossible to cut

##### Highly prohibited:
* Teflon (PTEE)
* Copper
* PVC
* Phénolic resion, epoxy
* Vinyl or imitation leather

## Machines
To understand how the machines in FabLab work, we have to understand that in order to cut something, the machine only recognizes a vectorized file. Preferably an SVG file.

### Pixel vs Vector
The main reason a vectorized file is required is that, while it can be geometrically deformed, changed, edited, or zoomed, it will never lose on image quality. As for a pixeled image, when undergoing transformation, its quality is highly impacted. This is also called distortion.

![](../images/Module_4/Vector_Pixel.jpg)
Photoshop from Adobe for example, works with pixelized images, which made sense because it's a pixel-editing program. Illustrator from adobe as well, on the other hand, works with vectors, because it's a geometrical-editing program designed for logos, etc.

### 1) Machine: Epilog Fusion Pro 
The Epilog Fusion Pro range is available in 3 different sizes. The description of these machines is available on their website [here](https://www.epiloglaser.com/laser-machines/fusion-pro-laser-series.htm). If I remember correctly, we have the Fusion Pro 36 in FabLab!

![](../images/Module_4/Epilog_FusionPro.jpg)


> Even if in the next exercise, I didn't work with the Epilog machine, I thought it might still be interesting to mention the different types of characteristics of this machine since it's manufactured in a factory and not "self-made".

> However for the last exercise, the one where we had to create a Lampshade, I used the Epilog, because the Lasersaur was occupied. I'll explain more about the Epilog in the second part of this week’s module!

### 2) Machine: Lasersaur
For the next exercise, the class got divided into different types of groups. I, for instance, worked with an open-source Laser machine built by FabLab, called "Lasersaur". [Here](http://fablab-ulb.be/workshop-montage-de-la-decoupeuse-laser-lasersaur/) you can see the different stages of the construction of the machine and how it all started! 

The Lasersaur ('saur' like in dinosaur), was built for a number of reasons. Just by looking into the price tag of a CNC machine, is it more profitable to create one yourself. However, I think the most useful thing about the Lasersaur is its size! 
* Cutting surface is about 122cm by 61cm.
* Maximum height is 12 cm.
* Maximum speed is 6000mm/minute.
* Laser Power is 100 Watt.
* Type of Laser is an Infrared Co2 Tube.

![](../images/Module_4/Lasersaur.jpg)

## First Exercise With the laser machine Lasersaur
For this exercise, we were told to play with the different types of laser thicknesses. To do so we started by opening the vector-editing program Illustrator and we created this strip (see below). When we were done creating the vectorized strip we exported and saved it into an SVG file.

![](../images/Module_4/polytestpliage.png)

On the strip is written 10 Watt, this means the power of the Laser itself. As you can see, the 10Watt is for the whole strip. Below we have the different types of speeds. 

The main goal is to be able to identify and experiment with the different kinds of folding in the plastic. It all depends on the plastic as well and since we are working with a PP plastic of 1mm of thickness for the second exercise, the purpose of this strip is to see how deep and fast the laser can cut but with the same power 10 Watt.

![](../images/Module_4/Coupe_1.jpg)

As you can see, on the first try we got a result where the strip was literally 'cut' of by the laser. However, this strip was too plain and without any real annotations. So, for the second try, we did write the different speeds: Going from 100 to 2300.

![](../images/Module_4/Coupe_2.jpg)

## Driveboard App
Since there is a FabLab built CNC laser machine, there is an app called "Driveboard App" developed and programmed by FabLab as well! 

First, insert your USB stick into the Raspberry next to the computer. Next, open your SVG file with the Driveboard App. You then are going to see the strip. 

For this next part, it's important to mention that a machine doesn't know the difference in speed or power if you don’t configure it. The way it understands it is by colour configuration! As you can see below, each line is a colour you'll need to give, in order for the machine to laser cut it the way you'd like it! There are written different Passes (Pass 1, Pass 2, Pass 3, etc.). How many passes there are depends on how many lines you select and give it a colour. So, it's variable.

While you are doing the colour configuration, you'll also have to manually give in the different speeds (F) and the constant power of the Laser itself.
* Pass 1: F=100 & %=10
* Pass 2: F=300 & %=10
* Pass 3: F=600 & %=10
* etc.

> Remember if you want your text to be visible on your strip, you'll need to give it a colour configuration, speed and power as well. In our case, we gave it: Text : F=800 & %=10.

The photo of the program here below shows another exercise of another group. Please discard their exercise (square) as it had nothing to do with our exercise. I just wanted to show, when everything is done and you are satisfied with the configuration, you'll need to place your plastic plate into the Lasersaur machine. Align and calibrate the laser with your plastic. When you are sure it's in the middle of the plastic sheet, close the machine's door and
* Click the big red button to turn on the machine (it's also the button that acts as an emergency stop)
* Turn on the 2 smoke extractors, which are located behind the machine.
* Turn on water cooling, located next to the machine.
* Open the compressed air supply valve, located on top of the machine. While open, this will make a lot of noise, it's normal, don’t be afraid!

Now click the "RUN" button on the Driveboard App and happy laser cutting!
In less than 5 minutes we had our strip!

> Remember to wait for the smoke to be extracted before opening the machine!

![](../images/Module_4/DriveboardApp.jpg)

## Part 2:
## Second Exercise: The Lamp - Light source 
For this exercise, we were told to create a lamp or a lampshade that would essentially be inspired by the object we chose at the beginning of the semester. To say the least, I needed somehow to get inspired by the computer holder I talked about in my previous modules.

Since in [Module_2](./module02.md) we were to redo the object in order to understand its shape, I started to understand why the edges of the computer holder were round. Its job was to get a good grip of the computer itself, so it doesn't slide off.

I started to rethink those edges as "hooks" because hooks are supposed to be holding something. Whether it's a jacket or even a fish? Either way, I quickly realized, by playing with gravity, the computer was actually "hooked" on those edges. This wasn't just a laptop holder; this was a hooked laptop. Without gravity a hook is useless. Can you imagine an astronaut hooking a jacked in space? No, to do so, they have straps.

![](../images/Module_4/Hooks.jpg)

### Idea and Inspiration:
The Idea that came after was to play with gravity. I knew I wanted something loose and floaty, just like a cloud in the sky. I quickly concluded I wanted to create a chain. Fixed on the top but loose on the bottom and it needed to hang. 

To do so I remembered back in high school, I did create a shirt made out of a chain of those little taps you find on cans to open them. Interestingly, [this website](https://pulltabarchaeology.com/) even talks about the whole anatomy and archaeology of can taps!  

Besides serving as opening cans, can taps actually make great jewelry and DIY projects. They are made out of aluminium, which is a light metal and easy to carry around. Their Holes in the middle also make a great assemblage chain!

![](../images/Module_4/CanTaps.jpg)

### Design and print:
Now that I understand the shape of a tap, I can begin to sketch the type of chain I want to make. It has to be a chain where the cans get hooked but also hook into other taps. 

##### Sketch and assemble
I started by doing a sketch (see below) of my chain. I thought of how I could assemble everything. How each Tap would intertwine together. On the left is one solo Tap. The assembly is easy. Each piece needed to be folded horizontally in the middle (orange) and they needed to be cut vertically on top (red). By doing so, we obtain a chain.

![](../images/Module_4/Taps_Sketch.jpg)

##### Working with gravity:
Since it's not the first time doing this chain, I somehow knew how the material would fall, but I never did this chain on a plastic material, which was one of the rules.

##### The material:
It's important to mention that the material we are using is a translucent polyethylene sheet made from polyethylene plastic. It is important to know the characteristics of the material. The weight and the plastic can affect the lamp itself but also the light source itself. It's also important to search early for an adequate lightbulb or LED strip because depending on how the lightbulb heats, it can melt the plastic if in close contact!

##### First tests with the Epilog Machine:
Like I said in the first part of this module, I mainly worked with the Lasersaur the first time. This time, however, while I wanted to try and use the Laser machine, it was occupied, which left me no other choice but to use the Epilog. I was happy to get familiarised with this machine because, compared with the Lasersaur, it was so much easier to use the Epilog! So let me explain my process while doing my tests:

I started by opening Adobe Illustrator to create my Can Tap. First, I measured a normal-sized Tap, which was about 1,5cm x 2,5 cm. I thought the dimensions were a little too small for the lamp, but also because it would take me an eternity to assemble all the pieces together. I decided I would double the size by 2, meaning each piece would be 3cm x 5cm.

Now that I had each piece next to each other, it was ready to save an SVG format. To do so in illustrator go to FILE > EXPORT > EXPORT AS > in FORMAT select SVG. You can save the file on a USB stick and insert it in the computer next to the Epilog Machine in FabLab. Remember, the Epilog Machine is manufactured in a factory, meaning it is user-friendly. It's programmed in a way to simplify the process of laser cutting.

The computer recognizes instantly the USB stick. Click on the format of your SVG file. It will automatically open with Inkscape on the computer. Here you'll need to do some changes. By clicking on the red circle, in contour style, you’ll need to change the thickness to 0.2mm. By clicking on the yellow circle, you'll need to change the size of your sheet. Document Properties > Choose the desired Format. This way your sketch is on the correct sheet.
Then click on FILE (Fichier) > PRINT (imprimer) > Select the good printer EPILOG and click ok. It won't print right away, but it will open in the official program called "Epilog Dashboard 2.2.5".

![](../images/Module_4/Tests/Reglage.jpg)

Remember with the Lasarsaur we had to give different colours for different cuts or folding? With the Epilog Dashboard Program, it's the same thing, except it's only done in the program itself. On top is a picture of the whole program itself.
For standard procedure: On autofocus choose "Palpeur" and separate them by colour this is important! As you can see by the two last pictures below, the frequency is always set at 100%. However, it's with the Speed and the speed and power we are going to play with!

##### Tests and Results
I did some tests beforehand, which showed me different types of results.
1. Test 1: 
For cutting: Frequency = 100%. Speed = 20%. Power = 80%. 
For Folding: Frequency = 100%. Speed = 30%. Power = 10%.
Results= Burned/ Black Plastic & difficulty folding.

2. Test 2: 
For cutting: Frequency = 100%. Speed = 40%. Power = 80%. 
For Folding: Frequency = 100%. Speed = 30%. Power = 10%.
Results= Plastic not burned but not cut correctly & still difficulty folding.

3. Test 3: Final Test Used for my final Print!
For cutting: Frequency = 100%. Speed = 40%. Power = 80%. 
For Folding: Frequency = 100%. Speed = 70%. Power = 40%.
Results= Plastic did not cut correctly I decided to cut the rest by hand, to let other people print their lamp! & Easy folding.

![](../images/Module_4/Tests/1.EpilogDashboard.jpg)

##### Ready to print
When everything is ready go ahead and click at the bottom right "print" (imprimer). What you just did is you sent the file into the machine itself. The Epilog won't start right away! 
Firstly, open the lid. You'll need to place your plastic sheet on the the very left top corner of the machine. Close the lid. Next, it's very important to turn on the air ventilation of the Epilog machine! (Just like the Lasersaur). Next to the computer click on the ON button of the machine, careful it will get loud. 
When everything is ready, click on your file on the screen of the Epilog machine, that you just send, and go ahead and start the laser! Here you can also see the minutes it'll take.

![](../images/Module_4/Tests/Print.jpg)![](../images/Module_4/Tests/8.EpilogDashboard.jpg)

##### Assembly
To create my final cut, I used the setup of Test 3. Even though the plastic wasn't cut correctly, I decided it would be best to let other people print their lamps. It took 23 minutes to laser cut the pieces! All there was left to do, was to manually cut where the machine failed to cut. Here below you can see the assembly of the chain!

![](../images/Module_4/Resultat/Assembly.jpg)

### Final Product
I mostly talked about the chain here, but it's important to mention that while I was doing all this work, I decided to create a gear form, essentially a circle with teeths that would act like hooks.

Below you can see the final result. The chain is "stretched" by the gear on top. As gravity pulls, the chain becomes looser at the end. Since there is no stretching at the bottom, the chain seems to loosen up.
This phenomenon is also visible on a basketball net.

![](../images/Module_4/Resultat/Final.jpg)

### Final Thoughts about my Lamp:
I'm proud of how everything turned out! I know that gravity would play a really big role. However, one thing I have to say is that I didn't fully respect the rules of this exercise. The number one rule was to create a lamp and use as little material as possible. 

![](../images/Module_4/Resultat/Material.jpg)

Just so you can imagine how much material I used: Only for the pieces, I needed one big sheet of translucent polyethylene plastic. On one sheet alone were 198 single pieces, and I still needed to make a top part for my lamp, a part that would essentially carry all the weight of the Can Tap chain!

I must say, I did more work for the chain itself than for the top part. That is why it seems like the top part is a totally different element of the chain. I knew that would be one of the main arguments the professor would do ad I know I could have done better if I only had the time to do so. Time was tight but is on me to manage it better next time! All in all, I'm proud of the results! It's a very fragile lamp, but it works!


## Useful Websites and Documents :
Websites:
1. [Epilog Machine GitLab](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/dc6cd87ffe1610ae9d5b1dd8bab55d222f78ae49/EpilogFusion.md)
2. [Epilog Laser Website](https://www.epiloglaser.com/laser-machines/fusion-pro-laser-series.htm)
3. [Lasersaur GitLab](http://fablab-ulb.be/workshop-montage-de-la-decoupeuse-laser-lasersaur/)
4. [Pull Tab Archaeology](https://pulltabarchaeology.com/)

Documents:
1. [Tap Can Part .svg](../stl_documents/Module_4/TapCan.svg)
2. [Top Gear Part .svg](../stl_documents/Module_4/Top_Gear.svg)